from __future__ import annotations
from airflow.utils.dates import days_ago

from airflow.decorators import dag, task

PROJECT_ID = "shawnho-looker-2024"
TOPIC_ID = "dag-topic-trigger"
SUBSCRIPTION = "dag-subscription"
BUCKET_NAME = "f854c35e1e24540f-composer-bucket"

default_args = {
    'owner': 'airflow',
    'start_date': days_ago(1),
    'email_on_failure': False,
    'email_on_retry': False
}

@dag(
    dag_id = "taskflow-verdict",
    dag_display_name = "DAG used to demonstrate k8s taskflow",
    schedule_interval=None,
    default_args=default_args,
    catchup=False,
    tags=['example'],
)
def process_verdict():
    @task
    def generate_filename():
        inpath = "gs://f854c35e1e24540f-composer-bucket/judgement0920.csv"
        return {
            "inpath": inpath,
        }
    
    @task.kubernetes(image="asia-east1-docker.pkg.dev/shawnho-looker-2024/shawnho-looker-2024/shawn-python:3.11", 
                     namespace="composer-user-workloads", 
                     in_cluster=False)
    def verdict_processor(generate_filename):
        from datetime import datetime
        import pandas
        import numpy as np
        import json
        from sql.gemini import gemini_predict, write_to_db, upload_to_gcs, gcp_config
        try:
            inpath = generate_filename["inpath"]
            print(f"verdict_processor from inpath: {inpath}")
            cases_df = pandas.read_csv(inpath)
        except Exception as e:
            print(f"Error reading CSV from {inpath}: {e}")
            return  # Stop processing if CSV reading fails
        PROMPT_EXTRACT_DEFENDANT_INFORMATION = """
        # SYSTEM
        你是一個台灣銀行的風險評估專員, 你正在為越南的客戶做評估

        你會拿到一份越南法院的判決書,
        從這份判決書裡面, 你要找出所有關於原告被告被害人以及案件本身的相關資訊, 以及其他跟金融犯罪有關的資訊
        作為你評估的依據

        ## Data Format

        資訊必須以JSON格式產生, 並包含以下的欄位:

        "案件編號": 案件編號,
        "金融犯罪": True | False | None,
        "判決日期": yyyy-MM-dd, // 如果只有年份, 則以yyyy-00-00表示
        "判決結果(VN)":,
        ”是否有罪": 根據判決內容，被告是否有罪: True | False | None,
        "被告姓名(VN)"://被告姓名的原文姓名.
                    //注意, 如果有稱謂, 放在稱謂欄位; 這裡只有姓名;
                    //如果是Bùi Văn Qu (Lỳ)或是Bùi Văn Qu (P)或其他類似表示
                    //則保留姓名, 將()內資訊代表的意義放到其他欄位中, 例如：
                    //因為Bùi Văn Qu (P)表示被告是女性, 則將性別欄位設為F. 本欄位則為Bùi Văn Qu
        "被告姓名(EN)"://被告姓名的英文姓名.
                    // 注意, 如果有稱謂, 放在稱謂欄位; 這裡只有姓名;
                    //如果是Bùi Văn Qu (Lỳ)或是Bùi Văn Qu (P)或其他類似表示
                    //則保留姓名, 將()內資訊代表的意義放到其他欄位中, 例如：
                    //因為Bùi Văn Qu (P)表示被告是女性, 則將性別欄位設為F. 本欄位則為Bui Van Qu
        "被告職業": "判例內容中提到的被告職業, 以","分隔"
        "被告稱謂": “例如先生, 女士等"
        "被告性別": Male | Female | N/A
        "被告出生日期": yyyy-MM-dd, // 如果只有年份, 則以yyyy-00-00表示
        "被告犯罪前科(VN)": // 原文中如果提到該被告有任何犯罪前科, 則將原文列在此
        "被告地址(VN)": // 被告目前的居住地址, 如果內文沒有提到目前居住地址, 則為None
        "被告戶籍地址(VN)": // 被告戶籍地址, 如果內文沒有提到戶籍地址, 則為None
        "被告公司名稱(VN)":  // 被告公司名稱, 如果內文沒有提到公司名稱, 則為None
        "被告電話號碼": 判例內容中如果有提到被告所擁有使用的電話號碼或是呼叫器號碼, 如果有多支電話號碼，則以 ","隔開 
        "被告email": 判例內容提到的被告所擁有使用email, 以 ","隔開; 
        "被告配偶姓名(VN)": 判例內容提到的提到的被告配偶姓名, 以 ","隔開; 
        "被告父母姓名(VN)": 判例內容提到的提到的被告父母姓名, 以 ","隔開; 
        "被告新式身分證號":判例內容提到的提到的新式身分證號
        "被告舊NIC":判例內容提到的提到的舊NIC

        例如：

        {
            "案件編號": "118/2018/HS-ST",
            "金融犯罪": true,
            "判決日期": "2018-04-06",
            "判決結果(VN)": "Phạm Văn C bị kết án 14 năm tù về tội Giết người, 17 năm tù về tội Mua bán trái phép chất ma túy và 2 năm tù về tội Tàng trữ trái phép vũ khí quân dụng, tổng hợp hình phạt là 30 năm tù. Mai Kim A bị kết án 14 năm tù về tội Mua bán trái phép chất ma túy và 6 tháng tù về tội Không tố giác tội phạm, tổng hợp hình phạt là 14 năm 6 tháng tù.",
            "是否有罪": true,
            "被告": [
                {
                    "被告姓名(VN)": "Phạm Văn C",
                    "被告姓名(EN)": "Pham Van C",
                    "被告職業": "Lao động tự do",
                    "被告稱謂": null,
                    "被告性別": "Male",
                    "被告出生日期": "1991-01-16",
                    "被告犯罪前科(VN)": "Bản án số 265/2012/HSST ngày 27/7/2012 TAND thành phố Thái Nguyên, tỉnh Thái Nguyên xử phạt 06 tháng tù, cho hưởng án treo, thử thách 12 tháng về tội “Tiêu thụ tài sản do người khác phạm tội mà có” (Đã được xóa án).",
                    "被告地址(VN)": "phố N H, xã V L, huyện Bắc Sơn, tỉnh Lạng Sơn",
                    "被告戶籍地址(VN)": "phố N H, xã V L, huyện Bắc Sơn, tỉnh Lạng Sơn",
                    "被告公司名稱(VN)": null,
                    "被告電話號碼": null,
                    "被告email": null,
                    "被告配偶姓名(VN)": "Quảng Thị Thu H",
                    "被告父母姓名(VN)": "Phạm Thanh H (đã mất), Nguyễn Thị N",
                    "被告新式身分證號": null,
                    "被告舊NIC": null
                },
                {
                    "被告姓名(VN)": "Mai Kim A",
                    "被告姓名(EN)": "Mai Kim A",
                    "被告職業": "Lao động tự do",
                    "被告稱謂": null,
                    "被告性別": "Female",
                    "被告出生日期": "1992-10-16",
                    "被告犯罪前科(VN)": null,
                    "被告地址(VN)": "164 Tiểu khu M K, thị trấn B S, huyện Bắc Sơn, tỉnh Lạng Sơn",
                    "被告戶籍地址(VN)": "164 Tiểu khu M K, thị trấn B S, huyện Bắc Sơn, tỉnh Lạng Sơn",
                    "被告公司名稱(VN)": null,
                    "被告電話號碼": null,
                    "被告email": null,
                    "被告配偶姓名(VN)": null,
                    "被告父母姓名(VN)": "Mai Sỹ T (đã mất), Phạm Thị H",
                    "被告新式身分證號": null,
                    "被告舊NIC": null
                }
            ]
        }

        ## 資料處理原則

        以上判決內容資料的處理原則如下

        * 姓名(EN): 將姓名翻譯成英文; 翻譯的範例:Phạm Văn C -> Pham Van C; Mai Kim A -> Mai Kim A;
            - 如果姓名欄位中可能包含了多位被告或原告, 需要將這些不同的原告或被個拆開各別處理
        * 地址(EN): 將地址翻譯成英文; 地址的區段必須由大到小, 例如:國家, 省, 市, 縣...等等;
            如果地址缺少某一區塊, 例如並未寫出省份, 則忽略; 如果有寫出該區塊, 例如奉天省, 則需要連"省“也翻譯出來
        ## 不要解釋, 只需給出JSON.

        ## 金融相關犯罪之定義
        金融相關犯罪之定義如下：
        金融犯罪涵蓋了在金融和經濟領域中發生的各種違反相關法律和規範的非法行為。 
        這類犯罪行為多樣，包括但不限於詐騙、醫療欺詐、內線交易、保險欺詐、盜竊、信任騙局、逃稅、賄賂、侵吞、身份盜竊、洗錢，
        以及偽造文書、生產假幣和冒牌貨。 這些犯罪行為可以由個人、法人實體，甚至是有組織的犯罪集團所實施

        ## 法院判決書內容
        """
        BATCH_SIZE = 3
        OUT_BUCKET  = "composer-taskflow-output-bucket"
        index = 0
        failed = []
        case_details = []
        case_links = []
        for key, value in cases_df.iterrows():
            start_time = datetime.now()
            if index > BATCH_SIZE:
                break
            else:
                case_content = value["PDF_CONTENT"]
                case_link = value["CASE_LINK"]
                sentence_name = value["SENTENCE_NAME"] if str(value["SENTENCE_NAME"]) != "nan" else ""
                case_status = value["CASE_STATUS"]
                court_location = value["COURT_LOCATION"]
                case_info = value["CASE_INFO"]
                case_type = value["CASE_TYPE"]
                case_name = value["CASE_NAME"]
                
                if case_link in case_links:
                    print(f"duplicated:{case_link}")
                    continue
                else:
                    index = index + 1
                    case_links.append(case_link)

                prompt = PROMPT_EXTRACT_DEFENDANT_INFORMATION + case_content
                print(f"=== {index} ===")
                print(f"{case_link}...")
                
                prediction = gemini_predict(prompt)
                if prediction != "":
                    try:
                        prediction = prediction.lstrip("```json").rstrip("```").replace("```", "").strip()
                        o = json.loads(prediction)
                        case_json = {
                            "case_link": case_link if case_link != np.nan and case_link != "NaN" and case_link is not None else "",
                            "case_status": case_status if case_status != np.nan and case_status != "NaN" and case_status is not None  else "",
                            "court_location": court_location if court_location != np.nan and court_location != "NaN" and court_location is not None  else "",
                            "case_info": case_info if case_info != np.nan and case_info != "NaN" and case_info is not None else "",
                            "case_type": case_type if case_type != np.nan and case_type != "NaN" and case_type is not None else "",
                            "case_name": case_name if case_name != np.nan and case_name != "NaN" and case_name is not None else "",
                            "sentence_name": sentence_name,
                            "case_details": o
                        }
                        case_details.append(case_json)
                        
                        print("=== END ===")
                    except Exception as e:
                        print(prediction)
                        print(f"[Error]{e}")
                        failed.append(f"{key}")
                else:
                    failed.append(f"{key}")
            end_time = datetime.now()
            total_timespan = end_time - start_time
            print(f"Time spent:{total_timespan.total_seconds()} seconds")

        config = {}
        config["gcp"] = gcp_config
        write_to_db(case_details=case_details, config=config, SQL_TABLE_NAME="cb.defendants_20241017")
        upload_to_gcs(OUT_BUCKET, "all-defendants-20241017-001.json", json.dumps(case_details, indent=2, ensure_ascii=False))
        upload_to_gcs(OUT_BUCKET, "failed-defendants-20241017-001.json", json.dumps(failed, indent=2, ensure_ascii=False))

    filename = generate_filename()
    verdict_processor(filename)

process_verdict()
