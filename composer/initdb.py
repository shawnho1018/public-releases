from sql.sql import DataConnection

# Create Config for DB
gcp_config = {}
gcp_config["database_user_name"] = "llmuser"
gcp_config["database_password_key"] = "orderdb-secret"
gcp_config["database_private_ip_address"] = "10.140.0.44"
gcp_config["database_public_ip_address"] = "34.81.184.151"
gcp_config["google-default-region"] = "asia-east1"
gcp_config["google-project-id"] = "shawnho-looker-2024"
gcp_config["postgres_instance_connection_name"] = "shawnho-looker-2024:asia-east1:composer-db"

config = {}
config["gcp"] = gcp_config

connection = DataConnection(config=config)

SQL_TABLE_PII_EMBEDDINGS_NAME = "cb.pii_embeddings_20241017"
SQL_TABLE_CASE_DETAILS_NAME = "cb.case_details_20241017"
SQL_TABLE_NAME = "cb.defendants_20241017"
USER=gcp_config["database_user_name"]

connection.execute("CREATE EXTENSION IF NOT EXISTS vector", sql_params=None)
connection.execute("CREATE EXTENSION IF NOT EXISTS pg_trgm", sql_params=None)
connection.execute("CREATE EXTENSION IF NOT EXISTS fuzzystrmatch", sql_params=None)

sql = f"""
create schema if not exists cb;
drop table if exists {SQL_TABLE_NAME};
create table if not exists {SQL_TABLE_NAME} (
    case_id TEXT DEFAULT NULL,
    link TEXT DEFAULT NULL,
    defendant_name TEXT DEFAULT NULL,
    defendant_name_en TEXT DEFAULT NULL,
    profile TEXT DEFAULT NULL,
    profile_embeddings VECTOR(768) DEFAULT NULL,
    address TEXT DEFAULT NULL,
    household_address TEXT DEFAULT NULL,
    address_en TEXT DEFAULT NULL,
    household_address_en TEXT DEFAULT NULL,
    birthdate TEXT DEFAULT NULL,
    company_name TEXT DEFAULT NULL,
    company_name_en TEXT DEFAULT NULL,
    phone_number     TEXT DEFAULT NULL,
    father_name TEXT DEFAULT NULL,
    mother_name TEXT DEFAULT NULL,
    spouse_name TEXT DEFAULT NULL,
    gender TEXT DEFAULT NULL,
    occupation TEXT DEFAULT NULL,
    occupation_embeddings VECTOR(768) DEFAULT NULL
);
GRANT USAGE ON SCHEMA cb TO {USER};
GRANT SELECT ON {SQL_TABLE_NAME} TO {USER};
GRANT INSERT ON {SQL_TABLE_NAME} TO {USER};
"""
connection.execute(sql, sql_params=None)

sql = f"""
create schema if not exists cb;
create table if not exists {SQL_TABLE_CASE_DETAILS_NAME} (
    case_id TEXT DEFAULT NULL,
    link TEXT DEFAULT NULL,
    pdf_content TEXT DEFAULT NULL,
    pdf_content_embedding VECTOR(768) NULL,
    pdf_content_bm25 VECTOR NULL,
    case_detail TEXT DEFAULT NULL,
    is_financial_case BOOL DEFAULT NULL,
    case_detail_zh TEXT DEFAULT NULL,
    case_status TEXT DEFAULT NULL,
    case_type TEXT DEFAULT NULL,
    case_name TEXT DEFAULT NULL,
    sentence_name TEXT DEFAULT NULL
);
GRANT USAGE ON SCHEMA cb TO llmuser;
GRANT SELECT ON {SQL_TABLE_CASE_DETAILS_NAME} TO llmuser;
GRANT INSERT ON {SQL_TABLE_CASE_DETAILS_NAME} TO llmuser;
"""
connection.execute(sql, sql_params=None)

sql = f"""
create schema if not exists cb;
create table if not exists {SQL_TABLE_PII_EMBEDDINGS_NAME} (
    case_id TEXT DEFAULT NULL,
    link TEXT DEFAULT NULL,
    defendant_name TEXT DEFAULT NULL,
    defendant_name_en TEXT DEFAULT NULL,
    address TEXT DEFAULT NULL,
    household_address TEXT DEFAULT NULL,
    address_en TEXT DEFAULT NULL,
    household_address_en TEXT DEFAULT NULL,
    birthdate TEXT DEFAULT NULL,
    company_name TEXT DEFAULT NULL,
    company_name_en TEXT DEFAULT NULL,
    phone_number     TEXT DEFAULT NULL,
    father_name TEXT DEFAULT NULL,
    mother_name TEXT DEFAULT NULL,
    spouse_name TEXT DEFAULT NULL,
    gender TEXT DEFAULT NULL,
    occupation TEXT DEFAULT NULL,
    defendant_name_en_embeddings VECTOR(768) DEFAULT NULL,
    phone_number_enembeddings VECTOR(768) DEFAULT NULL,
    father_name_enembeddings VECTOR(768) DEFAULT NULL,
    mother_name_enembeddings VECTOR(768) DEFAULT NULL,
    spouse_name_enembeddings VECTOR(768) DEFAULT NULL,
    gender_enembeddings VECTOR(768) DEFAULT NULL,
    address_enembeddings VECTOR(768) DEFAULT NULL,
    household_address_en_enembeddings VECTOR(768) DEFAULT NULL,
    birthdate_embeddings VECTOR(768) DEFAULT NULL,
    company_name_en_enembeddings VECTOR(768) DEFAULT NULL,
    occupation_embeddings VECTOR(768) DEFAULT NULL
);
GRANT USAGE ON SCHEMA cb TO llmuser;
GRANT SELECT ON {SQL_TABLE_PII_EMBEDDINGS_NAME} TO llmuser;
GRANT INSERT ON {SQL_TABLE_PII_EMBEDDINGS_NAME} TO llmuser;
"""
connection.execute(sql, sql_params=None)