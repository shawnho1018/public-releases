# # Verdict Processor on Google Cloud Composer

This project demonstrates an ETL pipeline using Google Cloud Composer (specifically Composer 1) to process Vietnamese court verdicts and extract key information.  It leverages Google Vertex AI's Gemini for text generation and embedding creation, and stores the extracted data in a Cloud SQL PostgreSQL database.

## Architecture

1. **Data Ingestion:** Court verdict data (currently in CSV format, `judgement0920.csv`) is stored in Cloud Storage.  A Cloud Function is triggered when new files are added to the designated bucket (`f854c35e1e24540f-composer-bucket`).  The function publishes a message to a Pub/Sub topic (`dag-topic-trigger`) containing details about the uploaded file.

2. **DAG Triggering:** The Composer DAG (`taskflow-verdict-origin`) is triggered by messages published to the Pub/Sub subscription (`dag-subscription`).

3. **Verdict Processing:** The DAG uses the Taskflow API to orchestrate the pipeline. The following tasks are performed:
    - **File Retrieval:** The DAG retrieves the CSV file from Cloud Storage based on the message from Pub/Sub.
    - **Data Extraction (Gemini):**  The core logic uses Gemini to process each verdict in the CSV. A prompt engineering approach extracts entities like defendant names, addresses, birthdates, case details, etc. and structures them into JSON.
    - **Address Normalization (Gemini):** Vietnamese addresses are normalized and translated into English using a dedicated Gemini prompt.
    - **Embedding Generation (Vertex AI):**  Defendant profiles and other textual data are embedded using a multilingual embedding model from Vertex AI.
    - **Data Storage (Cloud SQL):** Extracted data, including embeddings, is stored in a Cloud SQL PostgreSQL database.  The schemas `cb` are used to organize the tables. Tables include `cb.defendants_20241017`, `cb.case_details_20241017`, and `cb.pii_embeddings_20241017`.

## Setup

1. **Prerequisites:**
    - A Google Cloud Project with billing enabled.
    - A Composer 1 environment (`demo` in `asia-east1`).
    - A Cloud SQL PostgreSQL instance (`composer-db` in `asia-east1`).
    - A Cloud Storage bucket (`f854c35e1e24540f-composer-bucket`) for input files.
    - A Cloud Storage bucket (`composer-taskflow-output-bucket`) for output files (if needed).
    - Vertex AI API enabled.
    - Required Python libraries installed (see `requirements.txt`).
    - A Pub/Sub topic (`dag-topic-trigger`) and subscription (`dag-subscription`).
    - The `envs.sh` script configures environment variables. Source this file in your shell before running Composer commands.
    - Configure the database connection details in `initdb.py`.

2. **Database Initialization:** Run `initdb.py` to create the necessary database schema and tables.  This script sets up tables for storing defendant information, case details, and PII embeddings.

3. **Deployment:**
    - Package the DAG (`taskflow-verdict-origin.py`) and supporting files into a deployable archive (e.g., a zip file).
    - Upload the archive to the DAGs folder in the Cloud Storage bucket associated with your Composer environment (specified by `COMPOSER_FOLDER` in `envs.sh`).

4. **Triggering:**
    - Upload CSV files containing court verdict data to the input Cloud Storage bucket (`f854c35e1e24540f-composer-bucket`). This will trigger the Cloud Function, which will then trigger the DAG.


## Key Files

- `taskflow-verdict-origin.py`: The main DAG file.
- `requirements.txt`: Python library dependencies.
- `initdb.py`: Script for database initialization.
- `sql/sql.py`: Contains the `DataConnection` class for database interaction.
- `envs.sh`:  Environment variables setup script.


## Further Development

- **Composer 2 Migration:** Adapt the project for Composer 2, considering the changes in architecture and authentication.
- **Error Handling:** Improve error handling and logging within the DAG. The current version has basic error handling but could be more robust.
- **Scaling:** Consider strategies for scaling the pipeline to handle larger datasets.
- **Parameterization:** Use Airflow variables or environment variables to make the DAG more flexible and configurable.




