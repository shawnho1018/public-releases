export COMPOSER_FOLDER="gs://asia-east1-demo-800fbe16-bucket/dags"
export AIRFLOW_URL=$(gcloud composer environments describe demo \
    --location asia-east1 \
    --format='value(config.airflowUri)')
