from sql.sql import DataConnection
import json
from google.cloud import storage
import vertexai
from vertexai.generative_models import GenerativeModel, SafetySetting, GenerationConfig
from vertexai.language_models import TextEmbeddingInput, TextEmbeddingModel
from airflow.utils.dates import days_ago
import json
from airflow.decorators import dag, task

PROJECT_ID = "shawnho-looker-2024"
TOPIC_ID = "dag-topic-trigger"
SUBSCRIPTION = "dag-subscription"
BUCKET_NAME = "f854c35e1e24540f-composer-bucket"
OUT_BUCKET  = "composer-taskflow-output-bucket"

gcp_config = {
    'database_user_name': 'llmuser',
    'database_password_key': 'orderdb-secret',
    'database_private_ip_address': '10.140.0.44',
    'database_public_ip_address': '34.81.184.151',
    'google-default-region': 'asia-east1',
    'google-project-id': 'shawnho-looker-2024',
    'postgres_instance_connection_name': 'shawnho-looker-2024:asia-east1:composer-db'
}

def gemini_predict(prompt:str, generation_config:dict=None):
    print("gemini_predict")
    if generation_config is None:
        generation_config = GenerationConfig(
            max_output_tokens=8192,
            temperature=0,
            top_p=0.95,
        )

    safety_settings = [
        SafetySetting(
            category=SafetySetting.HarmCategory.HARM_CATEGORY_HATE_SPEECH,
            threshold=SafetySetting.HarmBlockThreshold.OFF
        ),
        SafetySetting(
            category=SafetySetting.HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT,
            threshold=SafetySetting.HarmBlockThreshold.OFF
        ),
        SafetySetting(
            category=SafetySetting.HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT,
            threshold=SafetySetting.HarmBlockThreshold.OFF
        ),
        SafetySetting(
            category=SafetySetting.HarmCategory.HARM_CATEGORY_HARASSMENT,
            threshold=SafetySetting.HarmBlockThreshold.OFF
        ),
    ]
    
    
    resp = ""
    try:
        vertexai.init(project=PROJECT_ID, location="us-central1")
        model = GenerativeModel(
            "gemini-1.5-pro-002"
        )
        responses = model.generate_content(
            [prompt],
            generation_config=generation_config,
            safety_settings=safety_settings,
            stream=True,
        )
        for response in responses:
                resp += response.text
    except Exception as e:
        print(f"[Error]{e}")

    return resp

def format_profile(defendant_name_en:str, addr_en:str, birthdate:str="", gender:str="",father_name:str="", mother_name:str="", spouse_name:str="") -> str:
    profile = f"""
{defendant_name_en} who lived in {addr_en} has a court case.
the person's birthdate is {birthdate}, gender is {gender}.
father is {father_name}, mother is {mother_name}, spouse is {spouse_name}
"""
    return profile

def embed_text(text:str, task_type:str="RETRIEVAL_DOCUMENT", model:str="text-multilingual-embedding-002") -> list:
    """Embeds texts with a pre-trained, foundational model.

    Returns:
        A list of lists containing the embedding vectors for each input text
    """
    if text is None or text.strip() == "":
        return None

    # A list of texts to be embedded.
    texts = [text]
    # The dimensionality of the output embeddings.
    dimensionality = 768 #256
    # The task type for embedding. Check the available tasks in the model's documentation.
    task = "RETRIEVAL_DOCUMENT"

    model = TextEmbeddingModel.from_pretrained(model)
    inputs = [TextEmbeddingInput(text, task) for text in texts]
    kwargs = dict(output_dimensionality=dimensionality) if dimensionality else {}
    embeddings = model.get_embeddings(inputs, **kwargs)

    return embeddings[0].values #[embedding.values for embedding in embeddings]

def normalize_address(address:str) -> str:
    PROMPT_NORMALIZE_ADDRESS = """
# SYSTEM
You are a Vietnam Household management officer.
Your task is to normalize any given Vietnam address

## Rules

* For any given Vietnam address.
You normalize the address to a FULL VALID address format.
Meanin the output is in a format from No, road...etc to Country
* Some of the section in the addreess may be missing, just ignore the missing parts.
* Numbers MUST be translated into English.
* Some Vietnam addresses may be shorten, for example: 405/10/13, you MUST to split it into correct normalized format.
For Example:
201/32/11 shoule be: Số nhà eleven Nằm trong hẻm thirty-two Hẻm two-zero-one


## Response Format

Respond with a string of the normalized address, DO NOT explain.

## Important

### Read the rules again before start thinking

### Think step by step.

## Vietnam Address Information

* 門牌號碼 số nhà
* 層       tầng (等同「lầu 」)
* 樓       lầu  (等同「tầng 」但lầu主要用在南越)
* 胡同    ngõ (等同「hẻm 」，多用於北越)
* 巷       hẻm (等同「ngõ  」，多用於南越)
* 路      đường
* 村       thôn  (等同「ấp」)
* 邑       ấp  (等同「thôn」)
* 鎮       thị trấn
* 鄉 / 社   xã
* 縣       huyện
* 市 / 城市 thành phố
* 省       tỉnh
* 國家    quốc gia


## Address to process

"""
    prompt = PROMPT_NORMALIZE_ADDRESS + address
    result_address = gemini_predict(prompt=prompt)
    print(result_address)
    return result_address

def write_to_db(case_details, config, SQL_TABLE_NAME):
    failed = []
    print("Write to DB")
    connection = DataConnection(config=config)
    connection.execute(f"delete from {SQL_TABLE_NAME}", sql_params=None)
    for case in case_details:
        try:
            for defendant in case["case_details"]["被告"]:
                # name_emb = json.dumps(convert_embeddings(get_textsim_embedding([name])[0]).tolist())
                case_id = case["case_details"]["案件編號"]
                link = case["case_link"]
                defendant_name = defendant["被告姓名(VN)"]
                defendant_name_en = defendant["被告姓名(EN)"]
                addr = defendant["被告地址(VN)"] if defendant["被告地址(VN)"] is not None else ""
                addr_en = defendant["被告地址(EN)"] if "被告地址(EN)" in defendant and defendant["被告地址(EN)"] is not None else ""
                if addr_en == "" and addr != "":
                    # Need Translation
                    translated_address = normalize_address(address=addr)
                    print(f"* translated_address={translated_address}")
                    defendant["被告地址(EN)"] = translated_address
                    addr_en = translated_address
                birthdate = defendant["被告出生日期"] if defendant["被告出生日期"] is not None and defendant["被告出生日期"] != {} else ""
                company_name = defendant["被告公司名稱(VN)"] if defendant["被告公司名稱(VN)"] is not None else ""
                company_name_en = defendant["被告公司名稱(EN)"] if "被告公司名稱(EN)" in defendant and defendant["被告公司名稱(EN)"] is not None and defendant["被告公司名稱(EN)"] != {} and defendant["公司名稱"]["origin"] is not None else ""
                phone_number = defendant["被告電話號碼"] if defendant["被告電話號碼"] is not None else ""
                phone_number_en = phone_number
                household_address = defendant["被告戶籍地址(VN)"]
                household_address_en = ""
                if household_address_en == "" and household_address != "" and household_address is not None:
                    # Need Translation
                    translated_household_address_en = normalize_address(address=household_address)
                    print(f"* translated_household_address_en={translated_household_address_en}")
                    defendant["被告戶籍地址(EN)"] = translated_household_address_en
                    household_address_en = translated_household_address_en
                if phone_number != []:
                    print(f"*** phone_number={phone_number}")
                profile = format_profile(defendant_name_en, addr_en)
                profile_embeddings = embed_text(text=profile, task_type="RETRIEVAL_DOCUMENT", model="text-multilingual-embedding-002")
                
                parents = defendant["被告父母姓名(VN)"]
                father_name = ""
                mother_name = ""
                if parents is not None and parents != "":
                    try:
                        father_name = parents.split(",")[0].strip()
                        mother_name = parents.split(",")[1].strip()
                    except Exception as e:
                        print(f"[Error]{e}")
                spouse_name = defendant["被告配偶姓名(VN)"] if defendant["被告配偶姓名(VN)"] is not None and defendant["被告配偶姓名(VN)"] != "" else ""
                gender = defendant["被告性別"] if defendant["被告性別"] is not None and defendant["被告性別"] != "" else ""
                occupation = defendant["被告職業"] if defendant["被告職業"] is not None and defendant["被告職業"] != "" else ""
                
                postgresql_sql = f"""
                    insert into {SQL_TABLE_NAME}(
                        case_id,
                        link,
                        defendant_name,
                        defendant_name_en,
                        profile,
                        profile_embeddings,
                        address,
                        address_en,
                        household_address,
                        household_address_en,
                        birthdate,
                        company_name,
                        company_name_en,
                        phone_number,
                        father_name,
                        mother_name,
                        spouse_name,
                        gender,
                        occupation)
                    values(
                        :case_id,
                        :link,
                        :defendant_name,
                        :defendant_name_en,
                        :profile,
                        :profile_embeddings,
                        :address,
                        :address_en,
                        :household_address,
                        :household_address_en,
                        :birthdate,
                        :company_name,
                        :company_name_en,
                        :phone_number,
                        :father_name,
                        :mother_name,
                        :spouse_name,
                        :gender,
                        :occupation
                    )"""
                sql_params = {
                    "link":link,
                    "case_id":case["case_details"]["案件編號"],
                    "defendant_name":defendant_name,
                    "defendant_name_en":defendant_name_en,
                    "address":addr,
                    "address_en":addr_en,
                    "household_address": household_address,
                    "household_address_en": household_address_en,
                    "birthdate":birthdate,
                    "company_name":company_name,
                    "company_name_en":company_name_en,
                    "phone_number":phone_number,
                    "profile": profile,
                    "profile_embeddings":f"{profile_embeddings}",
                    "father_name": father_name,
                    "mother_name": mother_name,
                    "spouse_name": spouse_name,
                    "gender": gender,
                    "occupation": occupation
                }
                print(f"{defendant_name}...")
                connection.execute(postgresql_sql, sql_params=sql_params)
        except Exception as e:
            print("===>>>")
            print(f"[Error]{e}")
            print(case)
            print("<<<===")
            defendant["case_id"] = case["case_details"]["案件編號"]
            failed.append(defendant)

def upload_to_gcs(bucket_name, blob_name, data):
    """Uploads data to a GCS bucket.

    Args:
        bucket_name: The name of the GCS bucket.
        blob_name: The name of the blob within the bucket.
        data: The data to upload (can be a string or bytes).
    """
    print("upload_gcs")
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(blob_name)
    blob.upload_from_string(data, content_type='application/json')
    print(f"Uploaded {blob_name} to gs://{bucket_name}/{blob_name}")

def handle_messages(pulled_messages, context):
    print(f"handle_messages: {pulled_messages}")
    process_files = list()
    for idx, m in enumerate(pulled_messages):
        data = m.message.data.decode("utf-8")
        message_data = json.loads(data)
        bucket_name = message_data['bucket']
        file_name = message_data['name']
        print(f"File {bucket_name}/{file_name} triggered the DAG")
        process_files.append(f"gs://{bucket_name}/{file_name}")
    
    return process_files

