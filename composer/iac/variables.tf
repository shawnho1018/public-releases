variable region {
    type = string
    default = "asia-east1"
}

variable project_id {
    type = string
    default = "shawnho-looker-2024"
}