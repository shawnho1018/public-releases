provider "google" {
  project = var.project_id
}

provider "google-beta" {
  project = var.project_id
}

resource "google_project_service" "composer_api" {
  service = "composer.googleapis.com"
  // Disabling Cloud Composer API might irreversibly break all other
  // environments in your project.
  // This parameter prevents automatic disabling
  // of the API when the resource is destroyed.
  // We recommend to disable the API only after all environments are deleted.
  disable_on_destroy = false
// this flag is introduced in 5.39.0 version of Terraform. If set to true it will
//prevent you from disabling composer_api through Terraform if any environment was
//there in the last 30 days
  #check_if_service_has_usage_on_destroy = true
}

resource "google_project_service" "pubsub_api" {
  service = "pubsub.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "functions_api" {
  service = "cloudfunctions.googleapis.com"
  disable_on_destroy = false
}

resource "google_pubsub_topic" "dag-trigger" {
  name    = "dag-topic-trigger"
}

resource "random_id" "bucket_prefix" {
  byte_length = 8
}

resource "google_storage_bucket" "bucket" {
  name  = "${random_id.bucket_prefix.hex}-composer-bucket"
  location = "US"
  uniform_bucket_level_access = true
}

resource "google_storage_bucket" "out_bucket" {
  name  = "composer-output-bucket"
  location = "US"
  uniform_bucket_level_access = true
}

data "google_storage_project_service_account" "gcs_account" {
}

// Create a Pub/Sub topic.
resource "google_pubsub_topic_iam_binding" "binding" {
  provider = google-beta
  topic    = google_pubsub_topic.dag-trigger.id
  role     = "roles/pubsub.publisher"
  members  = ["serviceAccount:${data.google_storage_project_service_account.gcs_account.email_address}"]
}

resource "google_storage_notification" "notification" {
  provider       = google-beta
  bucket         = google_storage_bucket.bucket.name
  payload_format = "JSON_API_V1"
  event_types    = ["OBJECT_FINALIZE", "OBJECT_METADATA_UPDATE"]
  topic          = google_pubsub_topic.dag-trigger.id
  depends_on     = [google_pubsub_topic_iam_binding.binding]
}

resource "google_pubsub_subscription" "dag-subscription" {
  name  = "dag-subscription"
  topic = google_pubsub_topic.dag-trigger.id

  # 20 minutes
  message_retention_duration = "1200s"
  retain_acked_messages      = true

  ack_deadline_seconds = 20

  expiration_policy {
    ttl = "300000.5s"
  }
  retry_policy {
    minimum_backoff = "10s"
  }

  enable_message_ordering    = false
}

## Composer Permission to Bigquery, GCS
data "google_service_account" "composer" {
  account_id = "composer-env-account"
}

resource "google_project_iam_member" "composer-gcs" {
  project = var.project_id
  role    = "roles/storage.objectUser"
  member  = "serviceAccount:${data.google_service_account.composer.email}"
}
resource "google_project_iam_member" "composer-genai" {
  project = var.project_id
  role    = "roles/aiplatform.user"
  member  = "serviceAccount:${data.google_service_account.composer.email}"
}

resource "google_project_iam_member" "composer-secret" {
  project = var.project_id
  role    = "roles/secretmanager.secretAccessor"
  member  = "serviceAccount:${data.google_service_account.composer.email}"
}

resource "google_project_iam_member" "composer-sql" {
  project = var.project_id
  role    = "roles/cloudsql.client"
  member  = "serviceAccount:${data.google_service_account.composer.email}"
}

resource "google_pubsub_topic" "topic" {
  name = "tf-topic"
}

data "google_project" "project" {}

resource "google_pubsub_topic_iam_member" "secrets_manager_access" {
  topic  = google_pubsub_topic.topic.name
  role   = "roles/pubsub.publisher"
  member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-secretmanager.iam.gserviceaccount.com"
}

resource "google_secret_manager_secret" "orderdb-secret" {
  secret_id = "orderdb-secret"
  replication {
    user_managed {
      replicas {
        location = var.region
      }
    }
  }
  topics {
    name = google_pubsub_topic.topic.id
  }

  rotation {
    rotation_period = "3600s"
    next_rotation_time = "2045-11-30T00:00:00Z"
  }

  depends_on = [
    google_pubsub_topic_iam_member.secrets_manager_access,
  ]
}

resource "google_secret_manager_secret_version" "orderdb-secret-value" {
  secret = google_secret_manager_secret.orderdb-secret.id

  secret_data = "password"
}

