from __future__ import annotations

from datetime import datetime
from sql.sql import DataConnection
import json
import pandas
import numpy as np
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.providers.google.cloud.operators.pubsub import (
    PubSubPullOperator,
)
from google.cloud import storage
from vertexai.generative_models import GenerativeModel, SafetySetting, GenerationConfig
from vertexai.language_models import TextEmbeddingInput, TextEmbeddingModel
from airflow.utils.dates import days_ago

PROJECT_ID = "shawnho-looker-2024"
TOPIC_ID = "dag-topic-trigger"
SUBSCRIPTION = "dag-subscription"
BUCKET_NAME = "f854c35e1e24540f-composer-bucket"
OUT_BUCKET  = "composer-output-bucket"
gcp_config = {}
gcp_config["database_user_name"] = "llmuser"
gcp_config["database_password_key"] = "orderdb-secret"
gcp_config["database_private_ip_address"] = "10.140.0.44"
gcp_config["database_public_ip_address"] = "34.81.184.151"
gcp_config["google-default-region"] = "asia-east1"
gcp_config["google-project-id"] = "shawnho-looker-2024"
gcp_config["postgres_instance_connection_name"] = "shawnho-looker-2024:asia-east1:composer-db"


def gemini_predict(prompt:str, generation_config:dict=None):
    print("gemini_predict")
    # response_schema = {
    #     "type": "object",
    #     "properties": {
    #         "案件編號": { "type": "string" },
    #         "金融犯罪": { "type": "boolean" },
    #         "判決日期": { "type": "string", "format": "date" },
    #         "判決結果(VN)": { "type": "string" },
    #         "是否有罪": { "type": "boolean" },
    #         "被告": {
    #             "type": "array",
    #             "items": {
    #                 "type": "object",
    #                 "properties": {
    #                     "被告姓名(VN)": { "type": "string" },
    #                     "被告姓名(EN)": { "type": "string" },
    #                     "被告職業": { "type": "string" },
    #                     "被告稱謂": { "type": ["string", "null"] },
    #                     "被告性別": { "enum": ["male", "female"] },
    #                     "被告出生日期": { "type": "string", "format": "date" },
    #                     "被告犯罪前科(VN)": { "type": "string" },
    #                     "被告地址(VN)": { "type": "string" },
    #                     "被告戶籍地址(VN)": { "type": "string" },
    #                     "被告公司名稱(VN)": { "type": ["string", "null"] },
    #                     "被告電話號碼": { "type": ["string", "null"] },
    #                     "被告email": { "type": ["string", "null"], "format": "email" },
    #                     "被告配偶姓名(VN)": { "type": ["string", "null"] },
    #                     "被告父母姓名(VN)": { "type": "string" },
    #                     "被告新式身分證號": { "type": ["string", "null"] },
    #                     "被告舊NIC": { "type": ["string", "null"] }
    #                 },
    #             }
    #         }
    #     }
    # }
    if generation_config is None:
        generation_config = GenerationConfig(
            max_output_tokens=8192,
            temperature=0,
            top_p=0.95,
        )

    safety_settings = [
        SafetySetting(
            category=SafetySetting.HarmCategory.HARM_CATEGORY_HATE_SPEECH,
            threshold=SafetySetting.HarmBlockThreshold.OFF
        ),
        SafetySetting(
            category=SafetySetting.HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT,
            threshold=SafetySetting.HarmBlockThreshold.OFF
        ),
        SafetySetting(
            category=SafetySetting.HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT,
            threshold=SafetySetting.HarmBlockThreshold.OFF
        ),
        SafetySetting(
            category=SafetySetting.HarmCategory.HARM_CATEGORY_HARASSMENT,
            threshold=SafetySetting.HarmBlockThreshold.OFF
        ),
    ]
    
    
    resp = ""
    try:
        model = GenerativeModel(
            "gemini-1.5-pro-002"
        )
        responses = model.generate_content(
            [prompt],
            generation_config=generation_config,
            safety_settings=safety_settings,
            stream=True,
        )
        for response in responses:
                resp += response.text
    except Exception as e:
        print(f"[Error]{e}")

    return resp

def format_profile(defendant_name_en:str, addr_en:str, birthdate:str="", gender:str="",father_name:str="", mother_name:str="", spouse_name:str="") -> str:
    profile = f"""
{defendant_name_en} who lived in {addr_en} has a court case.
the person's birthdate is {birthdate}, gender is {gender}.
father is {father_name}, mother is {mother_name}, spouse is {spouse_name}
"""
    return profile

def embed_text(text:str, task_type:str="RETRIEVAL_DOCUMENT", model:str="text-multilingual-embedding-002") -> list:
    """Embeds texts with a pre-trained, foundational model.

    Returns:
        A list of lists containing the embedding vectors for each input text
    """
    if text is None or text.strip() == "":
        return None

    # A list of texts to be embedded.
    texts = [text]
    # The dimensionality of the output embeddings.
    dimensionality = 768 #256
    # The task type for embedding. Check the available tasks in the model's documentation.
    task = "RETRIEVAL_DOCUMENT"

    model = TextEmbeddingModel.from_pretrained(model)
    inputs = [TextEmbeddingInput(text, task) for text in texts]
    kwargs = dict(output_dimensionality=dimensionality) if dimensionality else {}
    embeddings = model.get_embeddings(inputs, **kwargs)

    return embeddings[0].values #[embedding.values for embedding in embeddings]

def normalize_address(address:str) -> str:
    PROMPT_NORMALIZE_ADDRESS = """
# SYSTEM
You are a Vietnam Household management officer.
Your task is to normalize any given Vietnam address

## Rules

* For any given Vietnam address.
You normalize the address to a FULL VALID address format.
Meanin the output is in a format from No, road...etc to Country
* Some of the section in the addreess may be missing, just ignore the missing parts.
* Numbers MUST be translated into English.
* Some Vietnam addresses may be shorten, for example: 405/10/13, you MUST to split it into correct normalized format.
For Example:
201/32/11 shoule be: Số nhà eleven Nằm trong hẻm thirty-two Hẻm two-zero-one


## Response Format

Respond with a string of the normalized address, DO NOT explain.

## Important

### Read the rules again before start thinking

### Think step by step.

## Vietnam Address Information

* 門牌號碼 số nhà
* 層       tầng (等同「lầu 」)
* 樓       lầu  (等同「tầng 」但lầu主要用在南越)
* 胡同    ngõ (等同「hẻm 」，多用於北越)
* 巷       hẻm (等同「ngõ  」，多用於南越)
* 路      đường
* 村       thôn  (等同「ấp」)
* 邑       ấp  (等同「thôn」)
* 鎮       thị trấn
* 鄉 / 社   xã
* 縣       huyện
* 市 / 城市 thành phố
* 省       tỉnh
* 國家    quốc gia


## Address to process

"""
    prompt = PROMPT_NORMALIZE_ADDRESS + address
    result_address = gemini_predict(prompt=prompt)
    print(result_address)
    return result_address

def write_to_db(case_details, config, SQL_TABLE_NAME):
    failed = []
    print("Write to DB")
    connection = DataConnection(config=config)
    connection.execute(f"delete from {SQL_TABLE_NAME}", sql_params=None)
    for case in case_details:
        try:
            for defendant in case["case_details"]["被告"]:
                # name_emb = json.dumps(convert_embeddings(get_textsim_embedding([name])[0]).tolist())
                case_id = case["case_details"]["案件編號"]
                link = case["case_link"]
                defendant_name = defendant["被告姓名(VN)"]
                defendant_name_en = defendant["被告姓名(EN)"]
                addr = defendant["被告地址(VN)"] if defendant["被告地址(VN)"] is not None else ""
                addr_en = defendant["被告地址(EN)"] if "被告地址(EN)" in defendant and defendant["被告地址(EN)"] is not None else ""
                if addr_en == "" and addr != "":
                    # Need Translation
                    translated_address = normalize_address(address=addr)
                    print(f"* translated_address={translated_address}")
                    defendant["被告地址(EN)"] = translated_address
                    addr_en = translated_address
                birthdate = defendant["被告出生日期"] if defendant["被告出生日期"] is not None and defendant["被告出生日期"] != {} else ""
                company_name = defendant["被告公司名稱(VN)"] if defendant["被告公司名稱(VN)"] is not None else ""
                company_name_en = defendant["被告公司名稱(EN)"] if "被告公司名稱(EN)" in defendant and defendant["被告公司名稱(EN)"] is not None and defendant["被告公司名稱(EN)"] != {} and defendant["公司名稱"]["origin"] is not None else ""
                phone_number = defendant["被告電話號碼"] if defendant["被告電話號碼"] is not None else ""
                phone_number_en = phone_number
                household_address = defendant["被告戶籍地址(VN)"]
                household_address_en = ""
                if household_address_en == "" and household_address != "" and household_address is not None:
                    # Need Translation
                    translated_household_address_en = normalize_address(address=household_address)
                    print(f"* translated_household_address_en={translated_household_address_en}")
                    defendant["被告戶籍地址(EN)"] = translated_household_address_en
                    household_address_en = translated_household_address_en
                if phone_number != []:
                    print(f"*** phone_number={phone_number}")
                profile = format_profile(defendant_name_en, addr_en)
                profile_embeddings = embed_text(text=profile, task_type="RETRIEVAL_DOCUMENT", model="text-multilingual-embedding-002")
                
                parents = defendant["被告父母姓名(VN)"]
                father_name = ""
                mother_name = ""
                if parents is not None and parents != "":
                    try:
                        father_name = parents.split(",")[0].strip()
                        mother_name = parents.split(",")[1].strip()
                    except Exception as e:
                        print(f"[Error]{e}")
                spouse_name = defendant["被告配偶姓名(VN)"] if defendant["被告配偶姓名(VN)"] is not None and defendant["被告配偶姓名(VN)"] != "" else ""
                gender = defendant["被告性別"] if defendant["被告性別"] is not None and defendant["被告性別"] != "" else ""
                occupation = defendant["被告職業"] if defendant["被告職業"] is not None and defendant["被告職業"] != "" else ""
                
                postgresql_sql = f"""
                    insert into {SQL_TABLE_NAME}(
                        case_id,
                        link,
                        defendant_name,
                        defendant_name_en,
                        profile,
                        profile_embeddings,
                        address,
                        address_en,
                        household_address,
                        household_address_en,
                        birthdate,
                        company_name,
                        company_name_en,
                        phone_number,
                        father_name,
                        mother_name,
                        spouse_name,
                        gender,
                        occupation)
                    values(
                        :case_id,
                        :link,
                        :defendant_name,
                        :defendant_name_en,
                        :profile,
                        :profile_embeddings,
                        :address,
                        :address_en,
                        :household_address,
                        :household_address_en,
                        :birthdate,
                        :company_name,
                        :company_name_en,
                        :phone_number,
                        :father_name,
                        :mother_name,
                        :spouse_name,
                        :gender,
                        :occupation
                    )"""
                sql_params = {
                    "link":link,
                    "case_id":case["case_details"]["案件編號"],
                    "defendant_name":defendant_name,
                    "defendant_name_en":defendant_name_en,
                    "address":addr,
                    "address_en":addr_en,
                    "household_address": household_address,
                    "household_address_en": household_address_en,
                    "birthdate":birthdate,
                    "company_name":company_name,
                    "company_name_en":company_name_en,
                    "phone_number":phone_number,
                    "profile": profile,
                    "profile_embeddings":f"{profile_embeddings}",
                    "father_name": father_name,
                    "mother_name": mother_name,
                    "spouse_name": spouse_name,
                    "gender": gender,
                    "occupation": occupation
                }
                print(f"{defendant_name}...")
                connection.execute(postgresql_sql, sql_params=sql_params)
        except Exception as e:
            print("===>>>")
            print(f"[Error]{e}")
            print(case)
            print("<<<===")
            defendant["case_id"] = case["case_details"]["案件編號"]
            failed.append(defendant)

def upload_to_gcs(bucket_name, blob_name, data):
    """Uploads data to a GCS bucket.

    Args:
        bucket_name: The name of the GCS bucket.
        blob_name: The name of the blob within the bucket.
        data: The data to upload (can be a string or bytes).
    """
    print("upload_gcs")
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(blob_name)
    blob.upload_from_string(data, content_type='application/json')
    print(f"Uploaded {blob_name} to gs://{bucket_name}/{blob_name}")

def handle_messages(pulled_messages, context):
    print(f"handle_messages: {pulled_messages}")
    process_files = list()
    for idx, m in enumerate(pulled_messages):
        data = m.message.data.decode("utf-8")
        message_data = json.loads(data)
        bucket_name = message_data['bucket']
        file_name = message_data['name']
        print(f"File {bucket_name}/{file_name} triggered the DAG")
        process_files.append(f"gs://{bucket_name}/{file_name}")
    
    return process_files


def verdict_processor(inpath: str):
    # Vietnam verdict processor
    try:
        print("verdict_processor")
        cases_df = pandas.read_csv(inpath)
    except Exception as e:
        print(f"Error reading CSV from {inpath}: {e}")
        return  # Stop processing if CSV reading fails
    PROMPT_EXTRACT_DEFENDANT_INFORMATION = """
    # SYSTEM
    你是一個台灣銀行的風險評估專員, 你正在為越南的客戶做評估

    你會拿到一份越南法院的判決書,
    從這份判決書裡面, 你要找出所有關於原告被告被害人以及案件本身的相關資訊, 以及其他跟金融犯罪有關的資訊
    作為你評估的依據

    ## Data Format

    資訊必須以JSON格式產生, 並包含以下的欄位:

    "案件編號": 案件編號,
    "金融犯罪": True | False | None,
    "判決日期": yyyy-MM-dd, // 如果只有年份, 則以yyyy-00-00表示
    "判決結果(VN)":,
    ”是否有罪": 根據判決內容，被告是否有罪: True | False | None,
    "被告姓名(VN)"://被告姓名的原文姓名.
                //注意, 如果有稱謂, 放在稱謂欄位; 這裡只有姓名;
                //如果是Bùi Văn Qu (Lỳ)或是Bùi Văn Qu (P)或其他類似表示
                //則保留姓名, 將()內資訊代表的意義放到其他欄位中, 例如：
                //因為Bùi Văn Qu (P)表示被告是女性, 則將性別欄位設為F. 本欄位則為Bùi Văn Qu
    "被告姓名(EN)"://被告姓名的英文姓名.
                // 注意, 如果有稱謂, 放在稱謂欄位; 這裡只有姓名;
                //如果是Bùi Văn Qu (Lỳ)或是Bùi Văn Qu (P)或其他類似表示
                //則保留姓名, 將()內資訊代表的意義放到其他欄位中, 例如：
                //因為Bùi Văn Qu (P)表示被告是女性, 則將性別欄位設為F. 本欄位則為Bui Van Qu
    "被告職業": "判例內容中提到的被告職業, 以","分隔"
    "被告稱謂": “例如先生, 女士等"
    "被告性別": Male | Female | N/A
    "被告出生日期": yyyy-MM-dd, // 如果只有年份, 則以yyyy-00-00表示
    "被告犯罪前科(VN)": // 原文中如果提到該被告有任何犯罪前科, 則將原文列在此
    "被告地址(VN)": // 被告目前的居住地址, 如果內文沒有提到目前居住地址, 則為None
    "被告戶籍地址(VN)": // 被告戶籍地址, 如果內文沒有提到戶籍地址, 則為None
    "被告公司名稱(VN)":  // 被告公司名稱, 如果內文沒有提到公司名稱, 則為None
    "被告電話號碼": 判例內容中如果有提到被告所擁有使用的電話號碼或是呼叫器號碼, 如果有多支電話號碼，則以 ","隔開 
    "被告email": 判例內容提到的被告所擁有使用email, 以 ","隔開; 
    "被告配偶姓名(VN)": 判例內容提到的提到的被告配偶姓名, 以 ","隔開; 
    "被告父母姓名(VN)": 判例內容提到的提到的被告父母姓名, 以 ","隔開; 
    "被告新式身分證號":判例內容提到的提到的新式身分證號
    "被告舊NIC":判例內容提到的提到的舊NIC

    例如：

    {
        "案件編號": "118/2018/HS-ST",
        "金融犯罪": true,
        "判決日期": "2018-04-06",
        "判決結果(VN)": "Phạm Văn C bị kết án 14 năm tù về tội Giết người, 17 năm tù về tội Mua bán trái phép chất ma túy và 2 năm tù về tội Tàng trữ trái phép vũ khí quân dụng, tổng hợp hình phạt là 30 năm tù. Mai Kim A bị kết án 14 năm tù về tội Mua bán trái phép chất ma túy và 6 tháng tù về tội Không tố giác tội phạm, tổng hợp hình phạt là 14 năm 6 tháng tù.",
        "是否有罪": true,
        "被告": [
            {
                "被告姓名(VN)": "Phạm Văn C",
                "被告姓名(EN)": "Pham Van C",
                "被告職業": "Lao động tự do",
                "被告稱謂": null,
                "被告性別": "Male",
                "被告出生日期": "1991-01-16",
                "被告犯罪前科(VN)": "Bản án số 265/2012/HSST ngày 27/7/2012 TAND thành phố Thái Nguyên, tỉnh Thái Nguyên xử phạt 06 tháng tù, cho hưởng án treo, thử thách 12 tháng về tội “Tiêu thụ tài sản do người khác phạm tội mà có” (Đã được xóa án).",
                "被告地址(VN)": "phố N H, xã V L, huyện Bắc Sơn, tỉnh Lạng Sơn",
                "被告戶籍地址(VN)": "phố N H, xã V L, huyện Bắc Sơn, tỉnh Lạng Sơn",
                "被告公司名稱(VN)": null,
                "被告電話號碼": null,
                "被告email": null,
                "被告配偶姓名(VN)": "Quảng Thị Thu H",
                "被告父母姓名(VN)": "Phạm Thanh H (đã mất), Nguyễn Thị N",
                "被告新式身分證號": null,
                "被告舊NIC": null
            },
            {
                "被告姓名(VN)": "Mai Kim A",
                "被告姓名(EN)": "Mai Kim A",
                "被告職業": "Lao động tự do",
                "被告稱謂": null,
                "被告性別": "Female",
                "被告出生日期": "1992-10-16",
                "被告犯罪前科(VN)": null,
                "被告地址(VN)": "164 Tiểu khu M K, thị trấn B S, huyện Bắc Sơn, tỉnh Lạng Sơn",
                "被告戶籍地址(VN)": "164 Tiểu khu M K, thị trấn B S, huyện Bắc Sơn, tỉnh Lạng Sơn",
                "被告公司名稱(VN)": null,
                "被告電話號碼": null,
                "被告email": null,
                "被告配偶姓名(VN)": null,
                "被告父母姓名(VN)": "Mai Sỹ T (đã mất), Phạm Thị H",
                "被告新式身分證號": null,
                "被告舊NIC": null
            }
        ]
    }

    ## 資料處理原則

    以上判決內容資料的處理原則如下

    * 姓名(EN): 將姓名翻譯成英文; 翻譯的範例:Phạm Văn C -> Pham Van C; Mai Kim A -> Mai Kim A;
        - 如果姓名欄位中可能包含了多位被告或原告, 需要將這些不同的原告或被個拆開各別處理
    * 地址(EN): 將地址翻譯成英文; 地址的區段必須由大到小, 例如:國家, 省, 市, 縣...等等;
        如果地址缺少某一區塊, 例如並未寫出省份, 則忽略; 如果有寫出該區塊, 例如奉天省, 則需要連"省“也翻譯出來
    ## 不要解釋, 只需給出JSON.

    ## 金融相關犯罪之定義
    金融相關犯罪之定義如下：
    金融犯罪涵蓋了在金融和經濟領域中發生的各種違反相關法律和規範的非法行為。 
    這類犯罪行為多樣，包括但不限於詐騙、醫療欺詐、內線交易、保險欺詐、盜竊、信任騙局、逃稅、賄賂、侵吞、身份盜竊、洗錢，
    以及偽造文書、生產假幣和冒牌貨。 這些犯罪行為可以由個人、法人實體，甚至是有組織的犯罪集團所實施

    ## 法院判決書內容
    """
    BATCH_SIZE = 200
    index = 0
    failed = []
    case_details = []
    case_links = []
    for key, value in cases_df.iterrows():
        start_time = datetime.now()
        if index > BATCH_SIZE:
            break
        else:
            case_content = value["PDF_CONTENT"]
            case_link = value["CASE_LINK"]
            sentence_name = value["SENTENCE_NAME"] if str(value["SENTENCE_NAME"]) != "nan" else ""
            case_status = value["CASE_STATUS"]
            court_location = value["COURT_LOCATION"]
            case_info = value["CASE_INFO"]
            case_type = value["CASE_TYPE"]
            case_name = value["CASE_NAME"]
            
            if case_link in case_links:
                print(f"duplicated:{case_link}")
                continue
            else:
                index = index + 1
                case_links.append(case_link)

            prompt = PROMPT_EXTRACT_DEFENDANT_INFORMATION + case_content
            print(f"=== {index} ===")
            print(f"{case_link}...")
            
            prediction = gemini_predict(prompt)
            if prediction != "":
                try:
                    prediction = prediction.lstrip("```json").rstrip("```").replace("```", "").strip()
                    o = json.loads(prediction)
                    case_json = {
                        "case_link": case_link if case_link != np.nan and case_link != "NaN" and case_link is not None else "",
                        "case_status": case_status if case_status != np.nan and case_status != "NaN" and case_status is not None  else "",
                        "court_location": court_location if court_location != np.nan and court_location != "NaN" and court_location is not None  else "",
                        "case_info": case_info if case_info != np.nan and case_info != "NaN" and case_info is not None else "",
                        "case_type": case_type if case_type != np.nan and case_type != "NaN" and case_type is not None else "",
                        "case_name": case_name if case_name != np.nan and case_name != "NaN" and case_name is not None else "",
                        "sentence_name": sentence_name,
                        "case_details": o
                    }
                    case_details.append(case_json)
                    
                    print("=== END ===")
                    #blob_name = f"case-{index}.json"
                    #upload_to_gcs(OUT_BUCKET, blob_name, json.dumps(case_json, ensure_ascii=False))
                except Exception as e:
                    print(prediction)
                    print(f"[Error]{e}")
                    failed.append(f"{key}")
            else:
                failed.append(f"{key}")
        end_time = datetime.now()
        total_timespan = end_time - start_time
        print(f"Time spent:{total_timespan.total_seconds()} seconds")

    config = {}
    config["gcp"] = gcp_config
    write_to_db(case_details=case_details, config=config, SQL_TABLE_NAME="cb.defendants_20241017")
    upload_to_gcs(OUT_BUCKET, "all-defendants-20241017-001.json", json.dumps(case_details, indent=2, ensure_ascii=False))
    upload_to_gcs(OUT_BUCKET, "failed-defendants-20241017-001.json", json.dumps(failed, indent=2, ensure_ascii=False))

default_args = {
    'owner': 'airflow',
    'start_date': days_ago(1),
    'email_on_failure': False,
    'email_on_retry': False
}

# This DAG will run minutely and handle pub/sub messages by triggering target DAG
with DAG(
    "trigger-dag-verdict",
    schedule_interval="*/2 * * * *", # Runs every 2 minutes
    default_args=default_args,
    catchup=False, # Don't catch up on past missed runs
    description="A data pre-processing flow which integrates CSV into CloudSQL",
    tags=['verdict-processor'],
) as trigger_dag:
    
    # Pull messages from the Pub/Sub subscription
    pull_messages_operator = PubSubPullOperator(
        task_id="pull_messages_operator",
        project_id=PROJECT_ID,
        ack_messages=True, # Acknowledge messages after successful processing
        messages_callback=handle_messages, # Callback function to handle pulled messages
        subscription=SUBSCRIPTION,
    )
    # PythonOperator to process the verdicts received from the Pub/Sub messages.
    # It retrieves the processed files from XCom pushed by pull_messages_operator.
    def verdict_generator(**context):
        try:
            # Pulls the 'return_value' from the upstream task (pull_messages_operator)
            process_files = context['ti'].xcom_pull(key='return_value')
        except Exception as e:
            print(f"Error reading return_value: {e}")
            return # Exit if XCom pull fails
        # Processes each file if process_files is not empty
        if process_files:
            for file in process_files:
                print(f"Process file: {file}")
                verdict_processor(f"{file}") # Calls the main processing function
    
    # Defines the PythonOperator to run the verdict_generator function
    verdict_ai_operator = PythonOperator(
        task_id='verdict_generator',
        python_callable=verdict_generator,
        provide_context=True # Makes context available to the callable function
    )

    # Defines the task dependency: pull_messages_operator must complete before verdict_ai_operator starts
    pull_messages_operator >> verdict_ai_operator


#verdict_processor("gs://f854c35e1e24540f-composer-bucket/judgement0920.csv")