# Egress
這個專案提供一個UnitTest模式，進行Istio IngressGateway->Nginx->Egress NAT Gateway的整體End-to-End測試。

## 測試架構圖
![測試架構圖](./images/architecture.png)
主要分為三個部分，由Istio ingress gateway作為連線進口，nginx pod模擬服務轉導，通過Egress NAT Gateway連線到外部的Tomcat Server。

## 元件說明
### Istio Ingress Gateway
測試版本為asm-1.19.5-4，目前asm的安裝參數為：
```
#!/bin/bash
./asmcli install   --fleet_id shawnho-demo-2023   --kubeconfig ~/.kube/config   --output_dir ./asm-out   --platform multicloud   --enable_all   --ca mesh_ca --option envoy-access-log --option cloud-tracing --option stackdriver
```

Istio Gateway的安裝，使用asm安裝時所產出的asm-out folder
```
kubectl apply -f asm-out/samples/gateways/istio-ingressgateway -n gateway
```

關於Gateway 和 VirtualService的設定，主要記錄於proxy.yaml中，

### Nginx Proxy
作為轉呼叫的服務，對應的nginx pod需要重新打包。請在nginx folder底下執行
```
docker build -t [image-repository-location]
docker push [image-repository-location]
```
proxy的部署yaml，也詳列於proxy.yaml

### Egress NAT Gateway
預計配置兩個egress nat gateway，我們設定兩組NetworkGatewayGroup作為SNAT IP來源(0-ngg.yaml)，並指定兩組EgressNATPolicy(1-egress.yaml)，設定的技巧是部署時，我們部署兩組Deployment，但每個deployment帶著獨特的label (e.g. tier: one, tier: two)，讓EgressNATPolicy選擇不同的Deployment。這樣就可以讓不同的Pod，走各自的EgressNAT Gateway離開叢集。

若希望讓Egress NAT Gateway設定於不同的Worker Node，可在設定SNAT IP Address時(0-ngg.yaml)，通過調整snat ip個數與worker node節點個數的方式，將snat配發到正確的節點上。

### Tomcat Server
後端的Tomcat Server，需要額外以虛擬機設定。本文參考[以下文章](https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-10-on-ubuntu-20-04)進行的設定。


