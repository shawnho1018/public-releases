# Using K6 to stress test
K6是一個很小巧的壓測工具，可以快速提供開發者一個對於http or https服務的測試。保哥也有寫過[一篇介紹](https://blog.miniasp.com/post/2023/08/01/Getting-Started-with-Grafana-k6-Load-testing-tool)

## 下載K6
[引用文件](https://k6.io/docs/get-started/installation/)

在Mac上使用
```
brew install k6
```
在Linux (Debian/Ubuntu)上使用
```
sudo gpg -k
sudo gpg --no-default-keyring --keyring /usr/share/keyrings/k6-archive-keyring.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C5AD17C747E3415A3642D57D77C6C491D6AC1D69
echo "deb [signed-by=/usr/share/keyrings/k6-archive-keyring.gpg] https://dl.k6.io/deb stable main" | sudo tee /etc/apt/sources.list.d/k6.list
sudo apt-get update
sudo apt-get install k6
```
在Linux (CentOS)上使用
```
sudo dnf install https://dl.k6.io/rpm/repo.rpm
sudo dnf install k6
```

## 編輯script.js
k6 使用options來控制測試的變數，如加密憑證檢查，以及測試concurrent user量的壓力值。
下方參數我們定義了：不檢查加密憑證，不使用keep-alive，同時設定壓力10秒鐘成長到60 concurrent users, 1m到60,
之後10秒鐘的時間回到0。
```
export const options = {
  hosts: { 'frontend.shawnk8s.com': '35.244.250.121' },
  insecureSkipTLSVerify: true,
  stages: [
    { duration: '10s', target: 60 },
    { duration: '1m', target: 60 },
    { duration: '10s', target: 0 },
  ],
  thresholds: {
    http_req_duration: ['avg<100', 'p(95)<200'],
    http_req_failed: ['rate<0.01'],
  },
  noConnectionReuse: true,
};
```
function中定義了測試項目，我們分享了GET/POST的測試如下：
```
export default function () {
  const url = "https://frontend.shawnk8s.com"
  const payload = JSON.stringify({
    name: 'hello',
    message: 'sweet heart',
  });

  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  //http.post(url, payload, params);
  http.get(url)
}
```

## 驗證
底下動畫驗證一個自簽憑證的服務可以完整地被測試。
![測試](images/demo-k6-esun.gif)
