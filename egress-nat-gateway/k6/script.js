import { check } from 'k6';
import http from 'k6/http';

export const options = {
  hosts: { 'frontend.shawnk8s.com': '35.244.250.121' },
  insecureSkipTLSVerify: true,
  stages: [
    { duration: '10s', target: 30 },
    { duration: '10s', target: 60 },
    { duration: '10s', target: 0 },
  ],
  thresholds: { 
    http_req_duration: ['avg<100', 'p(95)<200'], 
    http_req_failed: ['rate<0.01'],
  },
//  noConnectionReuse: true,
};

export default function () {
  const url = "https://frontend.shawnk8s.com"
  const payload = JSON.stringify({
    name: 'hello',
    message: 'sweet heart',
  });

  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  http.get(url)
}

