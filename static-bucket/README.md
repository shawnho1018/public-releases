# Produce Signed Cookie Access with GCS
This project is to show customer how to configure signed cookie to restrict access to GCS. 

## Pre-requisite
No other pip libraries were required in python 3.11.
1. Setup signed key using 0-gen-key.sh
2. Create GCS and put a static file into the bucket
3. Configure GCS with LoadBalancer using 1-create-bs.sh

## How to run
Please using the following command line to generate the cookie.
--key_name: key used in 1-create-bs.sh
--url_prefix: LoadBalancer IP address 
--base64_key: content in signed-cookie
--expirtation_time: Epoch time (must be sometime after now)

An example: 
```
IPADDR=$(gcloud compute addresses describe example-ip  --format="get(address)" --global)
./signed-cookie.py sign-cookie --key_name=static-key --url_prefix=http://${IPADDR}/ --base64_key=OjpehTSXqOlnqs6_mczfXA== --expiration_time=1723785663
```

The result should output something like:
```
Cloud-CDN-Cookie=URLPrefix=aHR0cDovLzM0LjEwMi4xMjguMTY2Lw==:Expires=1723785663:KeyName=static-key:Signature=2VDxTZqezWiJRLPdZ2dCbrBJOQE=
```

Using curl to download the file
```
curl -svo test.pdf -H "Cookie: Cloud-CDN-Cookie=URLPrefix=aHR0cDovLzM0LjEwMi4xMjguMTY2Lw==:Expires=1723785663:KeyName=static-key:Signature=2VDxTZqezWiJRLPdZ2dCbrBJOQE=" http://34.102.128.166/2023_theo_cht.pdf 
```

