#!/bin/bash

GCS_BUCKET="shawnho-demo-static"
PROJECT_NUMBER=""
gsutil mb gs://$GCS_BUCKET

gsutil defacl set public-read gs://$GCS_BUCKET

gcloud compute backend-buckets create ${GCS_BUCKET} \
    --gcs-bucket-name=${GCS_BUCKET} \
    --enable-cdn \
    --cache-mode=CACHE_ALL_STATIC

gcloud compute backend-buckets \
   add-signed-url-key ${GCS_BUCKET} \
   --key-name static-key \
   --key-file signed-cookie


gsutil iam ch \
  serviceAccount:service-${PROJECT_NUMBER}@cloud-cdn-fill.iam.gserviceaccount.com:objectViewer \
  gs://BUCKET
#gcloud storage buckets add-iam-policy-binding gs://shawnho-demo-static --member=allUsers --role=roles/storage.objectViewer

gcloud compute addresses create example-ip --global

gcloud compute url-maps create http-lb \
    --default-backend-bucket=${GCS_BUCKET}

gcloud compute target-http-proxies create http-lb-proxy \
    --url-map=http-lb

gcloud compute forwarding-rules create http-lb-forwarding-rule \
    --load-balancing-scheme=EXTERNAL \
    --network-tier=PREMIUM \
    --address=example-ip \
    --global \
    --target-http-proxy=http-lb-proxy \
    --ports=80
