#!//Users/shawnho/ws2024/customers/cathay/static-bucket/.venv/bin/python
import argparse
import base64
from datetime import datetime
import hashlib
import hmac
from urllib.parse import parse_qs, urlsplit

def sign_cookie(
    url_prefix: str,
    key_name: str,
    base64_key: str,
    expiration_time: datetime,
) -> str:
    """Gets the Signed cookie value for the specified URL prefix and configuration.

    Args:
        url_prefix: URL prefix to sign.
        key_name: name of the signing key.
        base64_key: signing key as a base64 encoded string.
        expiration_time: expiration time.

    Returns:
        Returns the Cloud-CDN-Cookie value based on the specified configuration.
    """
    encoded_url_prefix = base64.urlsafe_b64encode(
        url_prefix.strip().encode("utf-8")
    ).decode("utf-8")
    epoch = datetime.utcfromtimestamp(0)
    expiration_timestamp = int((expiration_time - epoch).total_seconds())
    decoded_key = base64.urlsafe_b64decode(base64_key)

    policy = f"URLPrefix={encoded_url_prefix}:Expires={expiration_timestamp}:KeyName={key_name}"

    digest = hmac.new(decoded_key, policy.encode("utf-8"), hashlib.sha1).digest()
    signature = base64.urlsafe_b64encode(digest).decode("utf-8")

    signed_policy = f"Cloud-CDN-Cookie={policy}:Signature={signature}"

    return signed_policy

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    subparsers = parser.add_subparsers(dest="command")
    sign_cookie_parser = subparsers.add_parser(
        "sign-cookie",
        help="Generate a signed cookie to grant temporary authorized access.",
    )
    sign_cookie_parser.add_argument("--url_prefix", help="The URL prefix to sign.")
    sign_cookie_parser.add_argument("--key_name", help="Key name for the signing key.")
    sign_cookie_parser.add_argument(
        "--base64_key", help="The base64 encoded signing key."
    )
    sign_cookie_parser.add_argument(
        "--expiration_time",
        type=lambda d: datetime.utcfromtimestamp(float(d)),
        help="Expiration time expressed as seconds since the epoch.",
    )
    args = parser.parse_args()
    if args.command == "sign-cookie":
      print(
          sign_cookie(
              args.url_prefix, args.key_name, args.base64_key, args.expiration_time
          )
      )


