from locust import HttpUser, TaskSet, task, between
import time

class AwesomeUser(HttpUser):
    wait_time = between(10, 20)

    # On start populate our user list
    def on_start(self):
        self.createUsernames()

    # Create an array of user1 -> user1000
    def createUsernames(self):
        self.users = []
        for i in range(1000):
            self.users.append("user"+str(i+1))

    # Fetch the next user in our list
    def getUsername(self):
        return self.users.pop()


    # Login to the site
    def login(self):
        username = self.getUsername()
        loginInformation = {
            "username": username,
            "password":"passw0rd"
        }

        self.client.post("/", loginInformation)

    # Logout from the site
    def logout(self):
        self.client.post('/logout')

    # Define our tasks here
    @task(1)
    def load_page(self):
        # Login first
        self.login()

        # Put your logged in tasks here
        self.client.get('/profile')
        wait_time = between(1, 5)
        # Logout now
        self.logout()