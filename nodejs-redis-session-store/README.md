# User Session with Redis
這個專案是為了展示(For Demo Purpose Only)
1. 使用NodeJS的Express框架，來處理使用者登入、登出、Session維持。
2. 建置OpenTelemetry平台，監控服務與資料庫之間的連線狀態
3. 引入壓測以及Chaos Testing框架，確認監控數據均可在服務失效時發生其作用。

## Deploy GKE
terraform folder 提供了一個GKE的部署腳本，請修改terraform.tfvars對應的參數，並執行下方命令，即可部署。
```
terraform init && terraform apply
```
部署完成後，會一併啟動GKE Enterprise, Istio, 以及bindplane-ops manager。
其中bindplane-ops manager會包含預設帳號的設定部分，若要修改預設帳密，請修改terraform/bindplane-manifests/bindplane-secret.yaml裡的設定值（記得要base64 hash)

完成部署後，我們可通過gcloud container clusters get-credentials [cluster-name] 取得kubeconfig，檢視gateway以及bindplane名稱空間，應該都可以看到服務成功被部署如下（如範例）：
```
➜  gke-private git:(main) ✗ kubectl get pods,svc -n bindplane
NAME                                             READY   STATUS    RESTARTS   AGE
pod/bindplane-0                                  1/1     Running   0          4h39m
pod/bindplane-prometheus-0                       1/1     Running   0          4h39m
pod/bindplane-transform-agent-697b77fcd9-58pnq   1/1     Running   0          4h39m

NAME                                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/bindplane                   ClusterIP   192.168.32.55   <none>        3001/TCP   4h39m
service/bindplane-prometheus        ClusterIP   192.168.32.74   <none>        9090/TCP   4h39m
service/bindplane-transform-agent   ClusterIP   192.168.32.37   <none>        4568/TCP   4h39m
➜  gke-private git:(main) ✗ kubectl get pods,svc -n gateway
NAME                                      READY   STATUS    RESTARTS   AGE
pod/asm-ingressgateway-57b9cfb76c-8x6fm   1/1     Running   0          5d7h
pod/asm-ingressgateway-57b9cfb76c-qfqcw   1/1     Running   0          5d7h

NAME                         TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)                                      AGE
service/asm-ingressgateway   LoadBalancer   192.168.32.170   34.81.251.131   15021:32668/TCP,80:32086/TCP,443:32005/TCP   5d7h
```

部署成功後，我們以port-forward的方式，在localhost的3001 port檢視
```
kubectl port-forward svc/bindplane -n bindplane 3001:3001
```
預設的帳密: myuser/mypassword。

![Bindplane Ops Manager UI](imgs/bindplane-ui.png)

## Deploy the Code
這個服務使用Skaffold，並預定部署到K8S叢集(e.g GKE)中。想要部署這個服務，請修改skaffold.yaml 以及 k8s-manifests/deployments.yaml 所帶的image 名稱後，使用以下命令部署即可。
```
skaffold run
```

這個部署中，我們測試使用gateway api將服務發布至istio-gateway（對應yaml請參考k8s-manifests/istio.yaml）。
本服務自帶一個UI介面，可登入進行驗證。目前未綁定任何預設使用者，任何帳密皆可登入成功。

![UI 介面](imgs/login.png)

## 設定叢集監控
接下來，我們通過Bindplane Ops Manager UI設定本叢集的監控拓墣。點選UI中Configuration Tab，新增一個Configuration。並依照引導Wizard的步驟，依序設定Details，Source，以及Destination。
![Bindplane Config Setting UI](imgs/bindplane-config.png)
### Details
提供名稱，並選擇Platform為Linux。我們預計使用Linux的bindplane agent進行部署

### Source
由於我們程式碼中，會自動注入Trace並使用NodeJs prometheus-client middleware產生數據，對應的數據會被本Linux Agent所收集(prometheus用pulling, trace是被push進來)。我們選擇prometheus 以及opentelemetry兩個source。

* prometheus的設定：須提供Endpoint，可使用以下命令取得（e.g 192.168.17.188:8080)
```
kubectl get endpoints | grep session-handler
session-handler        192.168.17.188:8080                       3d18h
```
![source prometheus](imgs/source-prometheus.png)

* opentelemetry的設定：提供名稱，其餘使用預設即可

![source opentelemetry](imgs/source-otel.png)

### Destination
目前Bindplane的UI的設定Wizard只能夠設定一個Destination，我們先設定一組。等結束Wizard後，可以再新增一組。這裡我們先設定一組Google Managed Prometheus(GMP)，作為等等收容Prometheus Metrics的收集站。這裡主要提供名稱，Project ID,以及最重要的gcp service account key，提供的方式是匯出json後，將整個json貼入這個欄位中。這個key記得要先給予Cloud Monitoring Writer的權限。
![GMP Destination](imgs/dest-gmp.png)
設定完成後，我們按下Save 完成這個Configuration。
### 新增Destination
由於我們不僅需要GMP，還需要其他像Cloud Trace, Cloud Logging等，我們在剛設定好的configuration中，新增一個Destination: Google Cloud。輸入內容同GMP。
![New Destination in Configuration](imgs/dest-new-gcp.png)

## Deploy bindplane-agent
bindplane-agent folder提供了一個bindplane-agent的安裝機制。進入本folder，請到skaffold.yaml與 k8s-manifests/deployment.yaml這兩個檔案中，置換鏡像位置到對應專案下的artifact registry。更換完畢後，請使用以下命令將agent部署到與服務同個namespace中。
```
skaffold run
```
這個部署，其實只部署了ubuntu client以及一個內部的k8s service。目前的bindplane agent安裝script 無法直接在Dockerfile裡執行（仍在確認中）。目前僅能在部署成功後，通過kubectl exec進行手動部署。
在上述skaffold run部署完畢後，我們確認otel-receiver pod/service都成功部署完畢。
```
 kubectl get pods,svc | grep otel
pod/otel-receiver-5fddff5c9c-q59ds         1/1     Running   0            15h
service/otel-receiver          ClusterIP      192.168.32.118   <none>          4317/TCP,4318/TCP              15h
```
接下來我們到ops manager取得agent部署腳本。進入Ops Manager UI後，選擇AGENTS Tab，按下Install Agent。
### 選擇平台
我們選擇Linux，這時可以看到我們剛設定的Configuration出現在『Select Configuration』中。這時按下NEXT 我們就可以看到設定腳本如下圖：
![Agent安裝腳本](imgs/agent-script.png)
這時我們複製該命令，通過kubectl exec -it 讓他在otel-receiver pod中執行，如下例：
```
 kubectl exec -it otel-receiver-5fddff5c9c-q59ds  -- /bin/bash -c '/app/install_unix.sh -e ws://bindplane.bindplane.svc.cluster.local:3001/v1/opamp -s 353753ca-ae48-40f9-9588-28cf86430910 -v 1.45.0 -k configuration=demo,install_id=6f04b5cd-1ec5-4124-97a6-c607da900268'
```
安裝完畢後，我們可以回到Ops Manager，這時會看到Agent列表中出現otel-receiver pod名稱：
![bindplane-agent](imgs/otel-receiver.png)

我們進入Configuration，點選剛剛設定好的demo configuration。這時上方會出現一個Rollout Button，通過按下這個Button將我們的設定傳送到Bindplane Agent上，完成監控設定。
![bindplane-rollout](imgs/rollout.png)

## load-generator
這個服務中也提供了locust的壓測工具以及壓測腳本。目前壓測主要是登入、顯示、登出三個主要的動作。
部署壓測工具，請直接進入load-generator folder，並修改locust.yaml以及skaffold.yaml中的鏡像位置(e.g image)，通過skaffold run進行部署。

在locustfile的腳本中，我們使用httpuser的機制，直接模擬瀏覽器行為，會在登入後續的服務，均自帶傳回的session+cookie值於下個命令中。

## Chaos Mesh

### 安裝Chaos Mesh
為了驗證Redis與服務之間連線的impact，我們使用Chaos Mesh來進行。安裝工作我們使用helm
```
helm repo add chaos-mesh https://charts.chaos-mesh.org
kubectl create ns chaos-mesh
helm install chaos-mesh chaos-mesh/chaos-mesh -n=chaos-mesh --set chaosDaemon.runtime=containerd --set chaosDaemon.socketPath=/run/containerd/containerd.sock --version 2.6.3
```
安裝完畢後，登入服務，依照服務的UI Prompt設定RBAC。通過以下的命令取得ServiceAccount的Token，用以登入Chaos Mesh
```
kubectl create token [k8s-service-account-name]
```

### 部署對象服務
使用Chaos Mesh前，我們先對對象服務的Namespace進行Annotate。這是一個Chaos Mesh的保護機制，避免注入的錯誤，影響到非測試對象。
```
kubectl annotate ns default chaos-mesh.org/inject=enabled
```

### 注入錯誤
在這裏我們使用Chaos Mesh，所提供的K8S，將對象服務注入Network Error。我們在UI上進行以下選擇：
* Latency: 100ms
* Jitter: 100ms
* Correlation: 0
* Direction: Both
* Target: session-handler 
* Applied To: Redis

```
kind: NetworkChaos
apiVersion: chaos-mesh.org/v1alpha1
metadata:
  namespace: default
  name: demo
spec:
  selector:
    namespaces:
      - default
    labelSelectors:
      app: redis
  mode: all
  action: delay
  delay:
    latency: 100ms
    correlation: '0'
    jitter: 100ms
  direction: both
  target:
    selector:
      namespaces:
        - default
      labelSelectors:
        app: session-handler
    mode: all
```
按下Start開始相關測試
![Chaos Mesh](imgs/chaos-mesh.png]

### Load Generator
建議在開啟Network Error前，先由Load Generator 測試一版未有Latency的TPS以及Latency數值，用來和Latency開始後的數值比較。