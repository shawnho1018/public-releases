require('dotenv').config()
const promBundle = require("express-prom-bundle");
const metricsMiddleware = promBundle({includeMethod: true});
const express = require('express')
const flash = require('connect-flash')

const app = express()
const port = 8080
app.use(metricsMiddleware);
app.set('view engine', 'ejs')

const redis = require('redis')
var session = require('express-session')

let RedisStore = require('connect-redis')(session)
let redisClient = redis.createClient({
    host: 'redis-server',
    port: 6379
})
redisClient.on('error', (err) =>
  console.log(`Fail to connect to redis. ${err}`)
)
redisClient.on('connect', () => console.log('Successfully connect to redis'))
// Comment: Use the session middleware to manage user sessions
app.use(
  session({
    store: new RedisStore({ client: redisClient }),
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false, // if true only transmit cookie over https
        httpOnly: false, // if true prevent client side JS from reading the cookie 
        maxAge: 1000 * 60 * 1 // session max age in miliseconds
    }    
  })
)
// Comment: Use the flash middleware to display error messages
app.use(flash())
// Comment: Render the login page if the user is not authenticated
app.get('/', (req, res) => {
  if (req.session.isAuth) {
    return res.redirect('/profile')
  }
  const showInputError = req.flash('showInputError')[0] || false
  res.render('index', { showInputError })
})

// Comment: Middleware of parsing URL-encoded form data, for getting username and password
app.use(express.urlencoded({ extended: true }))
// Comment: Handle the login request and authenticate the user
app.post('/', (req, res) => {
  const { username, password } = req.body
  console.log( `session id: ${req.session.id}`)
  // Prevent empty input
  if (username.trim() === '' || password.trim() === '') {
    req.flash('showInputError', true)
    return res.redirect('/')
  }

  req.session.isAuth = true
  req.session.username = username
  req.session.timestamps = []
  res.redirect('/profile')
})

// Comment: Middleware function to check if the user is authenticated and add a timestamp to the session
const checkIsAuthAndAddTimestamp = (req, res, next) => {
  if (req.session.isAuth) {
    req.session.timestamps.push(new Date().getTime())
    next()
  } else {
    res.redirect('/')
  }
}
// Comment: Render the profile page if the user is authenticated
app.get('/profile', checkIsAuthAndAddTimestamp, (req, res) => {
  const { username, timestamps } = req.session
  redisClient.get(`sess:${req.session.id}`, (err, rawdata) => {
    if (err) {
      console.log(err)
    }
    //console.log('retrieved data:', rawdata);
  });
  res.render('profile', { username, timestamps })
})
// Comment: Handle the logout request and destroy the session
app.post('/logout', (req, res) => {
    //const sessions = redisClient.keys(`sess:${req.session.id}`);
    redisClient.del(`sess:${req.session.id}`);
    console.log(`logging out session: sess:${req.session.id}`)
    req.session.destroy((err) => {
        if (err) {
        return res.redirect('/profile')
        }
    })
    // Default name of cookie set by express-session
    res.clearCookie('connect.sid')
    res.redirect('/')
})

app.listen(port, () => {
  console.log(`Example app listening at http://0.0.0.0:${port}`)
})