from google.cloud import datastore
import datetime, argparse

def list_key_items(client: datastore.Client, kind_value):
    query = client.query(kind=kind_value)
    results = list(query.fetch())
    for result in results:
        print(result)

def add_entity(client: datastore.Client, kind_value):
  with client.transaction():
    timestamp = datetime.datetime.now(tz=datetime.timezone.utc).isoformat()
    key = client.key(
        kind_value, timestamp
    )

    task = client.get(key)

    if not task:
        task = datastore.Entity(key)
        task.update({"description": "Example task", "time": f"{timestamp}"})
        client.put(task)

def delete_items(client: datastore.Client, kind_value):
    query = client.query(kind=kind_value)
    results = list(query.fetch())
    for result in results:
        client.delete(result.key)

if __name__ == '__main__':
    client = datastore.Client()
    parser = argparse.ArgumentParser()
    parser.add_argument("--action", help="specify either add or delete")
    parser.add_argument("--keyvalue", help="specify the entity key value to add or delete")
    args = parser.parse_args()
    print(f"action: {args.action}, keyvalue: {args.keyvalue}")
    if args.action == "add":
      add_entity(client, args.keyvalue)
    elif args.action == "delete":
      delete_items(client, args.keyvalue)
    elif args.action == "list":
      list_key_items(client, args.keyvalue)
    else:
      printf(f"Not a supported action. Please use add/delete/list")
