# Datastore Key Operations
This script is to manage Datastore's key operations which compensates the current gcloud datastore cli missing parts.

## Prerequisite
1. Need to have (default) datastore created in GCP console. For terraform sample, you could view the main.tf under terraform folder.
2. Need to install google-cloud-datastore pypl in your client terminal with the following command:
```
pip install google-cloud-datastore
```
This script has been tested against google-cloud-datastore 2.19.0.

   If you're using python 3.11, I also provide requirements.txt to enlist all the libraries I used. You're welcome to test it using
```
pip install -r requirements.txt
```

## How to use
```
python demo.py --action add --keyvalue 'Shawn'
```
There are two parameters:

&nbsp; &nbsp; &nbsp; &nbsp; --action: now support add/list/delete, which provides the corresponding operations to the default datastore.

&nbsp; &nbsp; &nbsp; &nbsp; --keyvalue: the key value you hope to operate.


