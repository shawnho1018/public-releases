resource "google_firestore_database" "database" {
  project     = "shawnho-looker-2024"
  # google_datastore_index resources only support the (default) database.
  # However, google_firestore_index can express any Datastore Mode index
  # and should be preferred in all cases.
  name        = "(default)"
  location_id = "nam5"
  type        = "DATASTORE_MODE"

  delete_protection_state = "DELETE_PROTECTION_DISABLED"
  deletion_policy         = "DELETE"
}

resource "google_datastore_index" "default" {
  project = "shawnho-looker-2024"
  kind = "foo"
  properties {
    name = "property_a"
    direction = "ASCENDING"
  }
  properties {
    name = "property_b"
    direction = "ASCENDING"
  }

  depends_on = [google_firestore_database.database]
}
