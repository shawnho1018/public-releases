#!/bin/bash

function create-test-vm() {
  gcloud compute instances create swp-test-vm \
    --subnet=default \
    --zone=asia-east1-a \
    --image-project=debian-cloud \
    --image-family=debian-11 \
    --tags debian-client
}

function add-policy () {
cat << EOF > swp-policy.yaml
description: basic Secure Web Proxy policy
name: projects/shawnho-demo-2023/locations/asia-east1/gatewaySecurityPolicies/policy1
EOF

  gcloud network-security gateway-security-policies import policy1 \
    --source=./swp-policy.yaml \
    --location=asia-east1
}

function add-host-rule () {
cat << EOF > host-rule.yaml
name: projects/shawnho-demo-2023/locations/asia-east1/gatewaySecurityPolicies/policy1/rules/allow-shawnk8s-com
description: Allow shawnk8s.com
enabled: true
priority: 1
basicProfile: ALLOW
sessionMatcher: host() == 'demo.shawnk8s.com' || host() == '*.shawnk8s.com'
EOF

  gcloud network-security url-lists import allow-list \
    --location=asia-east1 \
    --project=shawnho-demo-2023 \
    --source=host-rule.yaml
}

function add-url-list () {
cat << EOF > allow-urllist.yaml 
name: projects/shawnho-demo-2023/locations/asia-east1/urlLists/example-org-allowed-list
values:
  - "demo.shawnk8s.com"
  - "*.shawnk8s.com"
  - "github.com/shawnho1018/*"
  - "api.myip.com"
EOF

  gcloud network-security url-lists import allow-list \
    --location=asia-east1 \
    --project=shawnho-demo-2023 \
    --source=allow-urllist.yaml
}

function create-swp () {
cat << EOF > swp-gateway.yaml
name: projects/shawnho-demo-2023/locations/asia-east1/gateways/swp1
type: SECURE_WEB_GATEWAY
addresses: ["10.140.0.99"]
ports: [443]
certificateUrls: ["projects/shawnho-demo-2023/locations/asia-east1/certificates/test-cert"]
gatewaySecurityPolicy: projects/shawnho-demo-2023/locations/asia-east1/gatewaySecurityPolicies/policy1
network: projects/shawnho-demo-2023/global/networks/default
subnetwork: projects/shawnho-demo-2023/regions/asia-east1/subnetworks/default
scope: samplescope
EOF

  gcloud network-services gateways import swp1 \
    --source=swp-gateway.yaml \
    --location=asia-east1
}

main () {
  create-test-vm
  add-policy
  create-swp    
  add-url-list
}

main "$@"
