#!/bin/bash
openssl req -x509 -newkey rsa:2048 \
  -keyout ./key.pem \
  -out ./cert.pem -days 365 \
  -subj '/CN=demo.shawnk8s.com' -nodes -addext \
  "subjectAltName=DNS:demo.shawnk8s.com"

gcloud certificate-manager certificates create test-cert \
   --certificate-file=./cert.pem \
   --private-key-file=./key.pem \
   --location=asia-east1
