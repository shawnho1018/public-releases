# Secure Web Proxy
Secure Web Proxy(SWP)是GCP提供客戶在VPC subnet中，用來作為HTTPS_PROXY的Managed L7 Proxy Server. 預設會使用一張TLS憑證，但本Proxy可支援憑證中間人，或是直接ByPass 加密封包兩類的使用。

使用時分為三個步驟：
## 提供憑證
提供一張自簽或是公發憑證，這裡0-sign-cert.sh 提供一個自簽憑證產生並上傳到cert-manager的腳本。

## 設定並創建SWP: 
執行1-deploy-swp.sh，這個腳本會：
* 建立Secure Web Proxy Policy: 這個Policy主要作為之後Rules的載體
* 創建Secure Web Proxy Gateway: 設定這個Gateway的IP，使用的憑證，gatewaySecurityPolicy（上步驟產生），以及生效的VPC Subnet。
* 創建一個Allowed URL List作為之後Rule使用

```
name: projects/shawnho-demo-2023/locations/asia-east1/urlLists/example-org-allowed-list
values:
  - "demo.shawnk8s.com"
  - "*.shawnk8s.com"
  - "github.com/shawnho1018/*"
  - "api.myip.com"
```

1-deploy-swp.sh 同時生成一個debian VM，可通過iap連線進入該虛擬機後。

## 測試：
進入虛擬機後，使用以下命令測試：
```
curl -x https://10.140.0.99 https://api.myip.com --proxy-insecure
```  

1. 無Rule: 連線會被拒絕，SWB預設沒有對應的Allow Rule，即會回覆403
```
curl: (56) Received HTTP code 403 from proxy after CONNECT
```
2. 執行 2-deploy-allow-url.sh 後再測試
若綁定以下rule成功
```
name: projects/shawnho-demo-2023/locations/asia-east1/gatewaySecurityPolicies/empty-policy/rules/allow-shawnk8s-com
basicProfile: ALLOW
enabled: true
priority: 1000
description: url-allow-list
sessionMatcher: inUrlList(host(), 'projects/shawnho-demo-2023/locations/asia-east1/urlLists/allow-list')
```

curl命令會回應：
```
{"ip":"35.185.138.206","country":"Taiwan","cc":"TW"}
```

3. 執行3-deploy-deny-src-url.sh 後再測試
此時，我們加上一個priority 更高，但為Deny的rule。用以阻擋測試機的VM IP
```
name: projects/shawnho-demo-2023/locations/asia-east1/gatewaySecurityPolicies/empty-policy/rules/allow-shawnk8s-com-src-32
basicProfile: DENY
enabled: true
priority: 100
description: url-allow-list
sessionMatcher: inIpRange(source.ip, '10.140.0.7/32') && inUrlList(host(), 'projects/shawnho-demo-2023/locations/asia-east1/urlLists/allow-list')
```
Rule綁定成功，即可看到curl回覆403
```
curl: (56) Received HTTP code 403 from proxy after CONNECT
```

## Source IP確認
SWB在創建時，會自動生成一組Cloud Gateway: swg-autogen-nat，並創建一個Cloud Router for NAT位置。可使用以下命令查詢：
```
router=$(gcloud compute routers list  --regions asia-east1  --filter="network:(default) AND name:(swg-autogen-router-*)" --format="get(name)")
gcloud compute routers get-status ${router}
```
Response:
```
kind: compute#routerStatusResponse
result:
  natStatus:
  - autoAllocatedNatIps:
    - 34.80.254.10
    - 35.185.138.206
    - 35.201.152.123
    - 35.201.242.126
    minExtraNatIpsNeeded: 0
    name: swg-autogen-nat
...
```
該Cloud Router也可以通過以下命令綁定預留IP Pool(建議至少5個)
```
gcloud compute routers nats update swg-autogen-nat  \
    --router=[Router] \
    --nat-external-ip-pool=IPv4_ADDRESSES... \
    --region=[Region]
```

底下提供GIF的![完整展示](images/demo-swp.gif)

## 清除SWB:
99-delete-swp.sh 可用來清除整個測試
