function add-url-rule () {

cat << EOF > urllist-src-32-rule.yaml
name: projects/shawnho-demo-2023/locations/asia-east1/gatewaySecurityPolicies/empty-policy/rules/allow-shawnk8s-com-src-32
basicProfile: DENY
enabled: true
priority: 100
description: url-allow-list
sessionMatcher: inIpRange(source.ip, '10.140.0.7/32') && inUrlList(host(), 'projects/shawnho-demo-2023/locations/asia-east1/urlLists/allow-list')
EOF

  gcloud network-security gateway-security-policies rules import allow-shawnk8s-com-src-32 \
    --source=urllist-src-32-rule.yaml \
    --location=asia-east1 \
    --gateway-security-policy=policy1
}

main () {
  add-url-rule
}

main "$@"
