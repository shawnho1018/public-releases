#!/bin/bash

# 清除SWP
gcloud network-services gateways delete --quiet swp1 --location=asia-east1

# 清除 Proxy Rules
gcloud network-security gateway-security-policies rules delete --quiet allow-shawnk8s-com --gateway-security-policy policy1 --location asia-east1
gcloud network-security gateway-security-policies rules delete --quiet allow-shawnk8s-com-src-32 --gateway-security-policy policy1 --location asia-east1

# 清除 Proxy
gcloud network-security gateway-security-policies delete --quiet policy1 \
    --location=asia-east1

# 清除 Self-Signed Cert for SWP
gcloud certificate-manager certificates delete --quiet test-cert --location asia-east1
