function add-url-rule () {

cat << EOF > urllist-rule.yaml
name: projects/shawnho-demo-2023/locations/asia-east1/gatewaySecurityPolicies/empty-policy/rules/allow-shawnk8s-com
basicProfile: ALLOW
enabled: true
priority: 1000
description: url-allow-list
sessionMatcher: inUrlList(host(), 'projects/shawnho-demo-2023/locations/asia-east1/urlLists/allow-list')
EOF

  gcloud network-security gateway-security-policies rules import allow-shawnk8s-com \
    --source=urllist-rule.yaml \
    --location=asia-east1 \
    --gateway-security-policy=policy1
}

main () {
  add-url-rule
}

main "$@"
