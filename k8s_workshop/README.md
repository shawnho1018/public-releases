# K8S Workshop Sharing
這是曾經辦給客戶的k8s知識分享，裡面包含了幾個部分：
1. K8S Authentication, Authorization, and Webhook: RBAC
2. NetworkPolicy
3. Ingress & Egress NAT Gateway
4. Istio
5. Pack Image Securely with distroless and Buildpack
6. Safely use Kubernetes Secret

主要的slide，可由[此處下載](doc/slide.pf)
