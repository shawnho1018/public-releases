# Use InitContainer to retrieve Secret in Secret Manager
This tutorial shows how to use initContainer to retrieve secrets from GCP secret manager. To test this project, please use the following command to create a username secret in Secret manager.
```
gcloud secrets create username
echo -n "shawnho" | gcloud secrets add username --data-file=-
```

Apply the following yaml to see how we retrieve the secret into a file in /etc/secret-volume/username

```
kubectl apply -f 1_pod-gcp-secret.yaml
```

[Note] Make sure you've used workload-identity to link the kubernetes service account to Google Service Account with Secret Manager Access permission.
