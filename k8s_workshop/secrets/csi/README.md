# Mount Google Cloud Secret Manager as CSI
This tutorial shows how to properly mount GCP secret manager object into a file into the pod. 
The following steps are required:

1. Install Secret CSI Driver using Helm Chart: 
   It can be done by running 
```
/bin/bash 0_install-csi-driver.sh
```

2. install CSI plugin for GCP secret manager
```
kubectl apply -f 1_provider-gcp-plugin.yaml
```

3. Configure the target secret as secretProviderClass. Don't forget to change the secrets as your own resource. The format of the resourceName is: projects/{your-project-id}/secrets/{your-secret-name}/versions/latests

```
kubectl apply -f 2_sc.yaml
``` 

4. Deploy pod and service account to mount the corresponding secretProviderClass in the desire path.

```
kubectl apply -f 3_service-account.yaml
kubectl apply -f 3_pod.yaml
```

Check to see there is a username.txt in the /var/secrets folder.

[Note] Make sure you've used workload-identity to link the kubernetes service account to Google Service Account with Secret Manager Access permission.
