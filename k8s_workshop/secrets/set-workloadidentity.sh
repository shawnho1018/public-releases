#!/bin/bash

export PROJECT_ID="shawnho-demo-2023"
export Google_SA="secret-accessor"
export K8S_SA="secret-access"
export K8S_NS="default"

function workload_identity() {
    if [ ! $# -eq 4 ]; then
      echo "Need 4 variables. In sequence: PROJECT_ID, GSA, K8S_NAMESPACE, KSA"
      return 1
    fi
    PROJECT_ID=$1
    GSA=$2
    K8S_NS=$3
    KSA=$4
    gcloud iam service-accounts add-iam-policy-binding ${GSA}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[${K8S_NS}/${KSA}]"

    kubectl annotate serviceaccount ${KSA} \
    --namespace ${K8S_NS} \
    iam.gke.io/gcp-service-account=${GSA}@${PROJECT_ID}.iam.gserviceaccount.com
}
workload_identity ${PROJECT_ID} ${Google_SA} ${K8S_NS} ${K8S_SA}
