# Network Policy Tutorial
This demo is to demonstrate the networkpolicy. 
First, we deploy a client and a server into the GKE cluster.
```
kubectl apply -f 0_netshoot.yaml
kubectl apply -f 0_nginx.yaml
```

We then use the following command to see we could successfully retrieve nginx website.
```
kubectl exec -it netshoot -- curl http://nginx
```

Then, we apply default deny ingress policy and retest the curl command above.
```
kubectl apply -f 1_default-deny-ingress.yaml
```

At last, we apply 2_allow-80.yaml to provide an allowed ingress policy. Then, we could again retest the curl command above. Since the allowed policy would overwrite the denied policy, we should expect the curl command works again. 
