# Ingress Demonstration
This tutorial demonstrates how to use GKE ingress. We also compared the 3 ingress configurations to expose kubernetes pod. 

1. We deploy 1_hello.yaml. We could use kubectl get svc to wait until the External IP address is shown and try to connect to it. We could expect a successful link to the hello deployment.

2. We then apply 2_ingress.yaml. This demonstrates how to link ingress to the target kubernetes service. 

3. The last demonstration is to place self-signed certificate onto ingress. This configuration also denied http link from 80 port.  

