#!/bin/bash
openssl req -new -newkey rsa:2048  \
    -nodes -x509 -subj '/CN=shawnk8s.com' -days 1800 \
    -keyout tls.key \
    -out tls.crt
kubectl create secret tls my-cert \
    --cert="tls.crt" --key="tls.key"
rm tls*
