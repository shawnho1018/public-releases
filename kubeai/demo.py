from langchain_openai import ChatOpenAI

llm = ChatOpenAI(
    model="gemma2-9b-gpu",
    temperature=0,
    max_tokens=8192,
    timeout=None,
    max_retries=2,
    api_key="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImIxZWE2ZWFiLWJhY2EtNGU1ZC04ZDNiLWVhNmM3YjI3MzkzNSJ9.3eAsIgY3PjvNqFdKOuncaq18KZAZhZG96_QaAdhvcyM",
    base_url="http://35.201.151.141/openai/v1",
)

messages = [
    (
        "system",
        "You are a helpful assistant that translates English to Chinese. Translate the user sentence.",
    ),
    ("human", "I love programming."),
]
ai_msg = llm.invoke(messages)
print(ai_msg.content)