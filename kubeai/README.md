# Deploy On-Premise Model
This project utilize [kubeai](https://github.com/substratusai/kubeai) to deploy models in the hugging face. Kubeai provides a kubernetes-native way to deploy models on either vLLM or Ollama. 

Kubeai also provides an Openai interface which could benefit from utilizing 

## Deploy Controller
1. Retrieve HUGGING_FACE_HUB_TOKEN from hugging face website
2. Deploy kubeai controller
```
helm repo add kubeai https://www.kubeai.org
helm repo update
curl -L -O https://raw.githubusercontent.com/substratusai/kubeai/refs/heads/main/charts/kubeai/values-gke.yaml

HUGGING_FACE_HUB_TOKEN=[retrieved from step 1]
helm upgrade --install kubeai kubeai/kubeai \
    -f values-gke.yaml \
    --set secrets.huggingface.token=$HUGGING_FACE_HUB_TOKEN \
    --wait
```
## Create Custom Model
The supported models in kubeai are listed in [this folder](https://github.com/substratusai/kubeai/tree/main/manifests/models). Since we planned to use Gemma2:9b (which has 8,192 context window. The largest among all models). We will create custom model yaml as shown below.
```
# kubeai-models.yaml
catalog:
  gemma2-9b-gpu:
    enable: true
```
Then install this new model into kubeai/models.
```
helm install kubeai-models kubeai/models  -f ./kubeai-models.yaml
```
## Deploy Custom Model
In the model yaml file, we specify the resourceProfile: nvidia-gpu-l4:1 implies 1 L4 GPU card. We also asked kubernetes to produce 1 replica for us.
```
cat << EOF | kubectl apply -f -
apiVersion: kubeai.org/v1
kind: Model
metadata:
  name: gemma2-9b-gpu
spec:
  features: [TextGeneration]
  owner: google
  url: ollama://gemma2:9b
  engine: OLlama
  resourceProfile: nvidia-gpu-l4:1
  minReplicas: 0
  replicas: 1
EOF
```

## Test with OpenAI API
We could use port-forward mechanism to expose kubeai service.
```
kubectl port-forward svc/kubeai 8080:80
```
Run the following test python process which utilize langchain-openai object.
```
python demo.py
```

## Test with openwebui
We could use port-forward mechanism to expose openwebui service to test using OpenAI Web UI.
```
kubectl port-forward svc/openwebui 8080:80
```
Using browser to connect to localhost:8080, we could view the following UI
![OpenWebUI](./images/openwebui.png)

Choose the model, gemma2-9b-gpu, on the top left corner. We could input our query to test the model.

