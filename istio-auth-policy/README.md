# Istio Authorization Demo (For Demo Purpose Only)
This demo is to demonstrate Istio's AuthorizationPolicy capability to redirect incoming http to a remote authenticator given a header information. 

## Create GKE & Install Managed ASM
Create GKE Cluster with managed ASM. Deploy asm-ingressgateway once the GKE cluster installation completed.

## Add istio-proxy mesh configuration
Add the following config into configmap, istio-asm-managed, in namespace istio-system.
```
data:
  mesh: |-
    # Add the following content to define the external authorizers.
    extensionProviders:
    - name: "sample-ext-authz-grpc"
      envoyExtAuthzGrpc:
        service: "ext-authz.demo.svc.cluster.local"
        port: "9000"
    - name: "sample-ext-authz-http"
      envoyExtAuthzHttp:
        service: "ext-authz.demo.svc.cluster.local"
        port: "8000"
        includeRequestHeadersInCheck: ["x-ext-authz"]
```

## Deploy Workload with External Authenticator
Execute install.sh to deploy all workloads into GKE.

## Test authenticator forwarding
We could use curl to test. The current configuration would redirect http request to external authenticator when visit a pre-defined subpath (e.g. root in this example). The request must carry a particular header: x-ext-authz: allow to be allowed entering the nginx pod. Otherwise, the request would be rejected. 

### Request Reject
```
curl -H "Host: nginx.shawnk8s.com" http://34.81.251.131;echo
denied by ext_authz for not found header `x-ext-authz: allow` in the request
```

### Request Allow
```
curl -H "x-ext-authz: allow" -H "Host: nginx.shawnk8s.com" http://34.81.251.131 ;echo
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

### Request Denined

```
curl -H "x-ext-authz: denied" -H "Host: nginx.shawnk8s.com" http://34.81.251.131 ;echo
denied by ext_authz for not found header `x-ext-authz: allow` in the request
```

The same request could also be applied even used with Kubernetes LoadBalancer.
